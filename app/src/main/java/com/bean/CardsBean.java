package com.bean;

import java.io.Serializable;

/**
 * Created by sujith on 8/1/2016.
 */
public class CardsBean implements Serializable {
    private int cardId = 0;
    private int selected = 0;
    private String cardYype = null;
    private String cardNo = null;

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getCardYype() {
        return cardYype;
    }

    public void setCardYype(String cardYype) {
        this.cardYype = cardYype;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
