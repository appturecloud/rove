package com.bean;

import java.io.Serializable;

/**
 * Created by sujith on 10-06-2016.
 */
public class CountryBean implements Serializable{
    private String name;
    private String flag;
    private String country_id = null;
    private String country_code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}
