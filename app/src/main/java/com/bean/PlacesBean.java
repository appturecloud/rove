package com.bean;

import java.io.Serializable;

/**
 * Created by sujith on 22-03-2016.
 */
public class PlacesBean implements Serializable{
    String place = null;
    String city = null;
    String description = null;
    String state = null;
    String country = null;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
