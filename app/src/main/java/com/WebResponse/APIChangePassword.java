package com.WebResponse;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 04-06-2016.
 */
@JsonObject
public class APIChangePassword implements Serializable {
    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private int code = 0;
    @JsonField(name = "msg")
    private String message = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
