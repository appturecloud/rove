package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 24-05-2016.
 */
@JsonObject
public class APIForgotPassword implements Serializable{
    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private int code = 0;
    @JsonField(name = "resetToken")
    private String resetToken = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }
}
