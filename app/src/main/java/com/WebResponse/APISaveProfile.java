package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 02-06-2016.
 */
@JsonObject
public class APISaveProfile implements Serializable {
    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private int code = 0;
    @JsonField(name = "verified_phone")
    private int phoneVerified = 0;
    @JsonField(name = "otp")
    private String otp = null;
    @JsonField(name = "msg")
    private String message = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status", status);
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        Log.e("code", code+" ");
        this.code = code;
    }

    public int getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(int phoneVerified) {
        Log.e("phone verified", phoneVerified+" ");
        this.phoneVerified = phoneVerified;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        Log.e("otp", otp);
        this.otp = otp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        Log.e("message", message);
        this.message = message;
    }
}
