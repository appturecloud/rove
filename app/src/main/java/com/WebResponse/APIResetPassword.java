package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by sujith on 24-05-2016.
 */
@JsonObject
public class APIResetPassword {

    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private int code = 0;
    @JsonField(name = "msg")
    private String msg = null;
    @JsonField(name = "firstName")
    private String fName = null;
    @JsonField(name = "lastName")
    private String lName = null;
    @JsonField(name = "email")
    private String email = null;
    @JsonField(name = "phone")
    private String phone = null;
    @JsonField(name = "id")
    private String uId = null;
    @JsonField(name = "key")
    private String key = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status ", status);
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        Log.e("meg", msg);
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}

