package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by sujith on 20-05-2016.
 */
@JsonObject
public class APILogin {

    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "id")
    private String id = null;
    @JsonField(name = "email")
    private String email = null;
    @JsonField(name = "phone")
    private String phone = null;
    @JsonField(name = "firstName")
    private String fname = null;
    @JsonField(name = "lastName")
    private String lname = null;
    @JsonField(name = "verified_phone")
    private int verified = 0;
    @JsonField(name = "otp")
    private String otp = null;
    @JsonField(name = "msg")
    private String msg = null;
    @JsonField(name = "key")
    private String key = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status is", status);
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        Log.e("id", id);
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        Log.e("mail", email);
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        Log.e("fname", fname);
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        Log.e("last name", lname);
        this.lname = lname;
    }

    public int getVerified() {
        return verified;
    }

    public void setVerified(int verified) {
        Log.e("verified", verified+" ");
        this.verified = verified;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        Log.e("otp", otp);
        this.otp = otp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        Log.e("key ", key);
        this.key = key;
    }
}
