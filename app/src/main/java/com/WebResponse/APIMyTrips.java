package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 7/14/2016.
 */
@JsonObject
public class APIMyTrips implements Serializable {

    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private int code = 0;
    @JsonField(name = "recordsFound")
    private int recordFound = 0;

//    @JsonField(name = "trip")
//    private String post = null;

    @JsonField(name = "trip")
    private List<APITripsList> tripsLists = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status", status);
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        Log.e("code is", code+" ");
        this.code = code;
    }

    public int getRecordFound() {
        return recordFound;
    }

    public void setRecordFound(int recordFound) {
        Log.e("record found", recordFound+" ");
        this.recordFound = recordFound;
    }

//    public String getPost() {
//        return post;
//    }
//
//    public void setPost(String post) {
//        this.post = post;
//    }
//
    public List<APITripsList> getTripsLists() {
        return tripsLists;
    }

    public void setTripsLists(List<APITripsList> tripsLists) {
        Log.e("size of the list", tripsLists.size() +" ");
        this.tripsLists = tripsLists;
    }
}
