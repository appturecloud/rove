package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 10-06-2016.
 */
@JsonObject
public class APIResendOTP implements Serializable{
    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "otp")
    private String otp = null;
    @JsonField(name = "code")
    private int code = 0;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status", status);
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
