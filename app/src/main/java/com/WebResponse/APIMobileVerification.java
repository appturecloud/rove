package com.WebResponse;

import android.util.Log;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 23-05-2016.
 */
@JsonObject
public class APIMobileVerification implements Serializable{

    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "msg")
    private String msg = null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        Log.e("message", msg);
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        Log.e("status", status);
        this.status = status;
    }
}
