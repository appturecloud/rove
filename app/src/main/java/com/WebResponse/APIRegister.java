package com.WebResponse;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by sujith on 20-05-2016.
 */

@JsonObject
public class APIRegister implements Serializable{

    @JsonField(name = "status")
    private String status = null;
    @JsonField(name = "code")
    private String code = null;
    @JsonField(name = "msg")
    private String message = null;
    @JsonField(name = "id")
    private String id = null;
    @JsonField(name = "otp")
    private String otp = null;
    @JsonField(name = "key")
    private String key = null;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
