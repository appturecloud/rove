package com.rovecab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.WebResponse.APITripsList;
import com.adapter.AdapterMyTrips;
import com.service.AsyncTaskListener;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 23-05-2016.
 */
public class ActivityMyTrip extends AppCompatActivity implements AsyncTaskListener {

//    ProgressDialog progressDialog = null;
    private ListView myTrips = null;
    private Toolbar toolbar = null;
    private LinearLayout layoutNoRequest = null;
    private LinearLayout progressbar = null;

    private String uId = null;
    private String key = null;
    private boolean home = false;
    private SharedPreferences mpref = null;
    private Context context = null;
    private AsyncTaskListener listener = null;
    private List<APITripsList> tripList = null;
    private List<NameValuePair> nameValuePairs = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trip);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        uId = mpref.getString("id", null);
        key = mpref.getString("key", null);

        myTrips = (ListView) findViewById(R.id.my_trips);
        layoutNoRequest = (LinearLayout) findViewById(R.id.txt_no_request);
        progressbar = (LinearLayout) findViewById(R.id.progressBar);

        listener = ActivityMyTrip.this;
        context = ActivityMyTrip.this;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            home = bundle.getBoolean("home");
        }

//        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("loading trips");
//        progressDialog.show();
        nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("uId", uId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, getString(R.string.BASE_URL) + "service/loadmytrips", nameValuePairs, 2, "mytrips").execute();

        myTrips.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myint = new Intent(ActivityMyTrip.this, ActivityTripsDetails.class);
                myint.putExtra("details", (Serializable) tripList.get(position));
                startActivity(myint);
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setMessage("loading trips");
//        progressDialog.show();
        progressbar.setVisibility(View.VISIBLE);
        layoutNoRequest.setVisibility(View.GONE);
        myTrips.setVisibility(View.GONE);
        nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("uId", uId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, getString(R.string.BASE_URL) + "service/loadmytrips", nameValuePairs, 2, "mytrips").execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                if (home) {
                    super.onBackPressed();
                } else {
                    Intent myint = new Intent(context, HomeActivity.class);
                    myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(myint);
                    ((Activity) context).finish();
                }
                break;
        }
        return true;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        progressbar.setVisibility(View.GONE);
        try {
            JSONObject jobj = new JSONObject(result);
            if (jobj != null) {
                if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
//                    progressDialog.dismiss();
                    JSONArray jsonArray = jobj.getJSONArray("trip");
                    if (jsonArray.length() > 0) {
                        layoutNoRequest.setVisibility(View.GONE);
                        myTrips.setVisibility(View.VISIBLE);
                        tripList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            APITripsList tripsList = new APITripsList();
                            tripsList.setTripId(jsonObject.getString("tripId"));
                            tripsList.setPickupTime(jsonObject.getString("pickupTime"));
                            tripsList.setTotalFare(jsonObject.getInt("totalFare"));
                            tripsList.setCarBrand(jsonObject.getString("carBrand"));
                            tripsList.setCarModel(jsonObject.getString("carModel"));
                            tripsList.setCarType(jsonObject.getString("carType"));
                            tripsList.setCurrencySign(jsonObject.getString("currencySign"));
                            tripsList.setCurrencyCode(jsonObject.getString("currencyCode"));
                            tripsList.setStatus(jsonObject.getString("status"));
                            tripsList.setCardNo(jsonObject.getString("cardNo"));
                            tripList.add(tripsList);
                        }
                        AdapterMyTrips adapter = new AdapterMyTrips(ActivityMyTrip.this, tripList);
                        myTrips.setAdapter(adapter);
                    }
                } else if (jobj.getString("status").equalsIgnoreCase("empty") && jobj.getInt("code") == 2) {
//                    progressDialog.dismiss();
                    layoutNoRequest.setVisibility(View.VISIBLE);
                    myTrips.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (home) {
            super.onBackPressed();
        } else {
            Intent myint = new Intent(context, HomeActivity.class);
            myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(myint);
            ((Activity) context).finish();
        }
    }
}
