package com.rovecab;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Dialog.SelectCountryDialog;
import com.WebResponse.APILogin;
import com.bean.CountryBean;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.DismissDialog;
import com.interfaces.WebServiceController;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sujith on 16-05-2016.
 */
public class ActivityLogin extends AppCompatActivity implements DismissDialog {

    private Toolbar toolbar = null;

    private CustomText txt_error_msg = null;
    private CustomEditText emailId = null;
    private CustomEditText password = null;
    private CustomEditText edtFlag = null;
    private CustomButton btnLogin = null;
    private LinearLayout forgotpassword = null;
    private LinearLayout errorLayout = null;
    private TextInputLayout txtInputLayout = null;

    private Retrofit retrofit = null;
    private WebServiceController webRequestController = null;

    private String email = null;
    private String phone = null;
    private String countryName = null;
    private String countryId = null;
    private List<CountryBean> countryBean = null;

    private SharedPreferences mpref = null;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressDialog = new ProgressDialog(ActivityLogin.this);
        progressDialog.setCancelable(false);
        countryBean = new ArrayList<>();

        countryBean = (ArrayList<CountryBean>) getIntent().getSerializableExtra("countries");


        emailId = (CustomEditText) findViewById(R.id.username);
        password = (CustomEditText) findViewById(R.id.password);
        btnLogin = (CustomButton) findViewById(R.id.login);
        forgotpassword = (LinearLayout) findViewById(R.id.forgotpassword);
        txtInputLayout = (TextInputLayout) findViewById(R.id.layout_flags);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txt_error_msg = (CustomText) findViewById(R.id.txt_error_msg);

        edtFlag = (CustomEditText) findViewById(R.id.edt_flag);
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);

        countryName = mpref.getString("cntry", null);

        for (int i = 0; i < countryBean.size(); i++) {
            if (countryBean.get(i).getName().equalsIgnoreCase(countryName)) {
                String uri = "@drawable/" + countryBean.get(i).getFlag();
                int imgRes = getResources().getIdentifier(uri, null, getPackageName());
                Drawable img = ActivityLogin.this.getResources().getDrawable(imgRes);
                img.setBounds(0, 0, 10, 10);
                edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                edtFlag.setText("+" + countryBean.get(i).getCountry_code());
                countryId = countryBean.get(i).getCountry_id();
                break;
            }
        }

        edtFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCountryDialog countryDialog = new SelectCountryDialog(ActivityLogin.this, countryBean, ActivityLogin.this);
                countryDialog.show();
            }
        });


        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent(ActivityLogin.this, ActivityForgotPassword.class);
                myint.putExtra("country", countryName);
                startActivity(myint);
            }
        });

        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    Long.parseLong(s.toString());
                    phone = s.toString();
//                    edtUserName.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(10)});
                    edtFlag.setVisibility(View.VISIBLE);
                } catch (NumberFormatException e) {
//                    edtUserName.setFilters(null);
                    email = s.toString();

//                    edtUserName.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(50)});
                    edtFlag.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCountryDialog selectCountryDialog = new SelectCountryDialog(ActivityLogin.this, countryBean, ActivityLogin.this);
                selectCountryDialog.show();
            }
        });
        final InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; ++i) {
                    if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890@._!#$%^&*/]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                        return "";
                    }
                }

                return null;
            }
        };


        emailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    Long.parseLong(s.toString());
                    phone = s.toString();
                    txtInputLayout.setVisibility(View.VISIBLE);
                } catch (NumberFormatException e) {
                    email = s.toString();
                    txtInputLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();
        webRequestController = retrofit.create(WebServiceController.class);

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    LoginApi();
                    return true;
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginApi();
            }
        });
    }

    @Override
    public void dismiss(Intent myint) {
        countryId = myint.getStringExtra("country_id");
        edtFlag.setText(myint.getStringExtra(getString(R.string.country)));
        String uri = "@drawable/" + myint.getStringExtra(getString(R.string.flag));
        int imgRes = getResources().getIdentifier(uri, null, getPackageName());
        Drawable img = ActivityLogin.this.getResources().getDrawable(imgRes);
        img.setBounds(0, 0, 10, 10);
        edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public boolean checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

    public void setSnackMessage(String message) {
        txt_error_msg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
            }
        });
    }

    public void LoginApi() {
        if (checkInternetConnection()) {
            if (emailId.getText().toString().length() > 0 && password.getText().toString().length() > 0) {
                progressDialog.setIndeterminate(false);
                progressDialog.setMessage("Loging in, Please wait");
                progressDialog.show();
                if (email == null) {
                    email = "";
                } else if (phone == null) {
                    phone = "";
                }
                Call<APILogin> loginResult = webRequestController.login(countryId, password.getText().toString(), phone, email);
                loginResult.enqueue(new Callback<APILogin>() {
                    @Override
                    public void onResponse(Response<APILogin> response, Retrofit retrofit) {
                        if (response.body().getStatus().equalsIgnoreCase("success") && response.body().getVerified() == 0) {
                            SharedPreferences.Editor edt = mpref.edit();
                            progressDialog.dismiss();
                            edt.putString("mobile", response.body().getPhone());
                            edt.putString("fname", response.body().getFname());
                            edt.putString("lname", response.body().getLname());
                            edt.putString("email", response.body().getEmail());
                            edt.putString("id", response.body().getId());
                            edt.putString("key", response.body().getKey());
                            edt.commit();

                            Intent myint = new Intent(ActivityLogin.this, ActivityVerification.class);
                            myint.putExtra("mobile", response.body().getPhone());
                            myint.putExtra("id", response.body().getId());
                            myint.putExtra("key", response.body().getKey());
                            startActivity(myint);

                        } else if (response.body().getStatus().equalsIgnoreCase("success") && response.body().getVerified() == 1) {
                            SharedPreferences.Editor edt = mpref.edit();
                            progressDialog.dismiss();
                            edt.putString("mobile", response.body().getPhone());
                            edt.putString("fname", response.body().getFname());
                            edt.putString("lname", response.body().getLname());
                            edt.putString("email", response.body().getEmail());
                            edt.putString("id", response.body().getId());
                            edt.putBoolean("login", false);
                            edt.putString("key", response.body().getKey());
                            edt.commit();

                            Intent myint = new Intent(ActivityLogin.this, ActivityVerification.class);
                            myint.putExtra("mobile", response.body().getPhone());
                            myint.putExtra("otp", response.body().getOtp());
                            myint.putExtra("id", response.body().getId());
                            myint.putExtra("forgot", false);
                            myint.putExtra("key", response.body().getKey());
                            startActivity(myint);
                        } else {
                            progressDialog.dismiss();
                            setSnackMessage(response.body().getMsg());
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            } else {
                setSnackMessage("Email or password cannot be empty.");
            }

        } else {
            setSnackMessage("Check your internet connection.");
        }

    }
}
