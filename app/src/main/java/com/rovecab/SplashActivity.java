package com.rovecab;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.adapter.PageAdapter;
import com.bean.CountryBean;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.HttpAsync;
import com.service.ScrollingImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by sujith on 11-05-2016.
 */
public class SplashActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener, com.google.android.gms.location.LocationListener, AsyncTaskListener {

    private GoogleApiClient googleApiClient = null;
    private LocationSettingsRequest.Builder builder = null;
    private LocationRequest locationRequest = null;
    private Handler handler = null;
    private Location location = null;
    private SharedPreferences mpref = null;
    private String country = null;
    private Context context = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;
    private List<NameValuePair> nameValuePairs = null;

    private ScrollingImageView scrolligImg1 = null;
    private ScrollingImageView scrolligImg2 = null;
    private ScrollingImageView scrolligImg3 = null;

    private int verifiedPhone = 0;
    private int DELAY = 1000;
    private String mobile = null;
    private String id = null;
    private String key = null;
    private ArrayList<CountryBean> bean = null;
    private Animation hyperspaceJump = null;
    private Animation fade_in = null;
    private LinearLayout layoutExtUser = null;
    private LinearLayout linearLayoutIndicator;
    private LinearLayout linearLayout = null;
    private CustomButton btnLogin = null;
    private CustomButton btnRegister = null;
    private ViewPager pager = null;
    private Geocoder geocoder = null;
    private List<Address> addresses = null;
    private int[] imageUrls = new int[]{R.drawable.bg, R.drawable.bg, R.drawable.bg, R.drawable.bg, R.drawable.bg};
    private List<ImageView> imageViewIndicatorList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        linearLayout = (LinearLayout) findViewById(R.id.status);
        pager = (ViewPager) findViewById(R.id.pager);

        hyperspaceJump = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.scaling_logo);
        fade_in = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fadeout_slogan);
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        handler = new Handler();
        layoutExtUser = (LinearLayout) findViewById(R.id.ext_users_splash);
        linearLayoutIndicator = (LinearLayout) findViewById(R.id.indicator_ll);
        btnLogin = (CustomButton) findViewById(R.id.login);
        btnRegister = (CustomButton) findViewById(R.id.register);
        scrolligImg1 = (ScrollingImageView) findViewById(R.id.scrolling_img1);
        scrolligImg2 = (ScrollingImageView) findViewById(R.id.scrolling_img2);
        scrolligImg3 = (ScrollingImageView) findViewById(R.id.scrolling_img3);

        listener = SplashActivity.this;
        context = SplashActivity.this;

        checkUsers();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent(SplashActivity.this, ActivityLogin.class);
                myint.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                myint.putExtra("countries", (Serializable) bean);
                myint.putExtra("country", country);
                startActivity(myint);
//                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent(SplashActivity.this, ActivityRegister.class);
                myint.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                myint.putExtra("countries", (Serializable) bean);
                startActivity(myint);
            }
        });

        fade_in.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                findViewById(R.id.status).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(SplashActivity.this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();


            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);


            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();

                    switch (status.getStatusCode()) {

                        case LocationSettingsStatusCodes.SUCCESS:
                            callSingleLocationProvider();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(
                                        SplashActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {

                                e.printStackTrace();
                            }
                            break;

                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("result code", requestCode+", "+ resultCode);
        switch (resultCode) {
            case 0:
                this.finish();
                break;

            case 1:
                break;

            case -1:
                callSingleLocationProvider();
                break;
        }
    }

    private void handler() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mpref.getBoolean("login", false)) {

                    progressDialog = new ProgressDialog(SplashActivity.this);
                    progressDialog.setMessage("Fetching details");
                    progressDialog.setIndeterminate(false);

                    nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("uId", mpref.getString("id", null)));
                    nameValuePairs.add(new BasicNameValuePair("key", mpref.getString("key", null)));

                    new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/syncprofile", nameValuePairs, 2, "user-info").execute();

                } else {
                    bean = new ArrayList<CountryBean>();
                    StringBuilder builder = new StringBuilder();
                    try {
                        InputStream inputStream = getAssets().open("phone.txt");
                        BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                        String str;
                        while ((str = buffer.readLine()) != null) {
                            builder.append(str);
                        }

                        inputStream.close();

                        JSONObject jobj = new JSONObject(builder.toString());
                        if (jobj != null) {
                            JSONArray jarr = jobj.getJSONArray("countries");
                            if (jarr.length() > 0) {
                                for (int i = 0; i < jarr.length(); i++) {
                                    JSONObject jsonObject = jarr.getJSONObject(i);
                                    CountryBean bean1 = new CountryBean();
                                    bean1.setName(jsonObject.getString("country_name"));
                                    bean1.setCountry_id(jsonObject.getString("id"));
                                    bean1.setCountry_code(jsonObject.getString("country_code"));
                                    bean1.setFlag(jsonObject.getString("country_flag"));
                                    bean.add(bean1);
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    findViewById(R.id.status).startAnimation(fade_in);

                }
            }
        }, DELAY);
    }

    private void callSingleLocationProvider() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_SMS}, 1000);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//            checkUsers();
            callSingleLocationProvider();
        }

        if(grantResults[0] == PackageManager.PERMISSION_DENIED){
            finish();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        SharedPreferences.Editor editor = mpref.edit();
        editor.putString("lat", String.valueOf(location.getLatitude()));
        editor.putString("lng", String.valueOf(location.getLongitude()));
        editor.commit();
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        new GetLocationAsync(location.getLatitude(), location.getLongitude()).execute();
        handler();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void initIndicator() {
        imageViewIndicatorList = new ArrayList<ImageView>();
        for (int i = 0; i < imageUrls.length; i++) {
            ImageView iv = new ImageView(this);
            iv.setLayoutParams(new ViewGroup.LayoutParams(30, 8)); //width, height
            iv.setPadding(3, 0, 3, 0);
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            if (i == 0) {
                iv.setImageResource(R.drawable.splash_indicator_focused);
            } else {
                iv.setImageResource(R.drawable.splash_indicator);
            }
            imageViewIndicatorList.add(iv);
            linearLayoutIndicator.addView(iv);
        }
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if(jobj!=null){
                if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){

                    mobile = jobj.getString("phone");
                    SharedPreferences.Editor edt = mpref.edit();
                    edt.putString("mobile", jobj.getString("phone"));
                    edt.putString("fname", jobj.getString("firstName"));
                    edt.putString("lname", jobj.getString("lastName"));
                    edt.putString("email", jobj.getString("email"));
                    edt.putString("country", String.valueOf(jobj.getInt("code")));
                    edt.commit();

                    verifiedPhone = jobj.getInt("verified_phone");


                    if(verifiedPhone == 1){
                        Intent myint = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(myint);
                        finish();
                    }
                    else{
                        Intent myint = new Intent(SplashActivity.this, ActivityVerification.class);
                        myint.putExtra("mobile", mobile);
                        myint.putExtra("id", mpref.getString("id", null));
                        myint.putExtra("key", mpref.getString("key", null));
                        startActivity(myint);
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class IPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < imageViewIndicatorList.size(); i++) {
                if (position == i) {
                    imageViewIndicatorList.get(i).setImageResource(
                            R.drawable.splash_indicator_focused);
                } else {
                    imageViewIndicatorList.get(i).setImageResource(
                            R.drawable.splash_indicator);
                }
            }
        }
    }

    public void checkUsers() {

        if (mpref.getBoolean("login", false)) {
//            pager.setVisibility(View.GONE);
            linearLayoutIndicator.setVisibility(View.GONE);
            findViewById(R.id.status).setVisibility(View.GONE);
            layoutExtUser.setVisibility(View.VISIBLE);

            scrolligImg1.setVisibility(View.GONE);
            scrolligImg2.setVisibility(View.GONE);
            scrolligImg3.setVisibility(View.GONE);


        } else {
//            pager.setVisibility(View.VISIBLE);
            linearLayoutIndicator.setVisibility(View.VISIBLE);
            layoutExtUser.setVisibility(View.GONE);
            scrolligImg1.setVisibility(View.VISIBLE);
            scrolligImg2.setVisibility(View.VISIBLE);
            scrolligImg3.setVisibility(View.VISIBLE);

            PageAdapter adapter = new PageAdapter(getSupportFragmentManager(), imageUrls);
            pager.setAdapter(adapter);
            pager.addOnPageChangeListener(new IPageChangeListener());
            initIndicator();
        }

    }



    private class GetLocationAsync extends AsyncTask<String, Void, String> {

        // boolean duplicateResponse;
        double x, y;
        StringBuilder str;

        public GetLocationAsync(double latitude, double longitude) {
            // TODO Auto-generated constructor stub

            x = latitude;
            y = longitude;
        }

        @Override
        protected String doInBackground(String... params) {

            geocoder = new Geocoder(SplashActivity.this, Locale.ENGLISH);
            try {
                addresses = geocoder.getFromLocation(x, y, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            str = new StringBuilder();
            if (geocoder.isPresent()) {

                if (addresses != null) {
                    Address returnAddress = addresses.get(0);

                    String localityString = returnAddress.getLocality();
                    String city = returnAddress.getCountryName();
                    country = city;
                    String region_code = returnAddress.getCountryCode();
                    String zipcode = returnAddress.getPostalCode();
                    str.append(localityString + "");
                    str.append(city + "" + region_code + "");
                    str.append(zipcode + "");

                    SharedPreferences.Editor edt = mpref.edit();
                    edt.putString("cntry", country);
                    edt.commit();
                }

            } else {
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {

        }

    }
}
