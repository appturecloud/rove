package com.rovecab;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.adapter.ListingAdapter;
import com.bean.PlacesBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 22-03-2016.
 */
public class ListAddress extends AppCompatActivity {

    private EditText edt_places = null;
    private ListView listPlaces = null;
    private ImageView imgLocationIndicator = null;
    private List<PlacesBean> bean = null;

    private String browserKey = "AIzaSyDLkJPLf-aIdaCeRbTLgvLYMJuw5d_FJSo";
    private String hint = null;
    private boolean lction = false;


    private List<String> sugestion = null;
    String url = null;
    private String lat = null;
    private String lng = null;
    private ArrayAdapter<String> adpt = null;
    public static String location_addr = null;
    private Bundle bundle = null;
    private Location location = null;


//    private Retrofit retrofit = null;
//    private WebServiceController webController = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_location);
        imgLocationIndicator = (ImageView) findViewById(R.id.location_indicator);
        edt_places = (EditText) findViewById(R.id.edt_place);
        listPlaces = (ListView) findViewById(R.id.listing);
        bean = new ArrayList<>();

        bundle = getIntent().getExtras();
        if (bundle != null) {
            location = bundle.getParcelable("location");
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
            hint = bundle.getString("hint");
            edt_places.setHint(hint);
        }

        if(hint.equalsIgnoreCase("Enter pickup location")){
            lction = true;
        }
        else if(hint.equalsIgnoreCase("Enter drop location")){
            lction = false;
        }

        if(hint.equalsIgnoreCase("Enter drop location")){
            imgLocationIndicator.setImageResource(R.drawable.drop_red);
        }
        else{
            imgLocationIndicator.setImageResource(R.drawable.pickup_blue);
        }

//        retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/").addConverterFactory(LoganSquareConverterFactory.create()).build();
//        webController = retrofit.create(WebServiceController.class);


        listPlaces.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try{
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    location_addr = bean.get(position).getDescription();
                    Intent myint = new Intent();
                    myint.putExtra("location", location_addr);
                    myint.putExtra(getString(R.string.city), bean.get(position).getCity());
                    myint.putExtra(getString(R.string.country), bean.get(position).getCountry());
                    myint.putExtra("icon", lction);
                    HomeActivity act = new HomeActivity();
                    act.onActivityResult(100, 100, myint);
//                    setResult(RESULT_OK, myint);
                    finish();
                }catch (Exception e){

                }
            }
        });

        edt_places.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAddress(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void listAddress(String s) {

        if (s.toString().contains(" ")) {
            s = s.toString().replace(" ", "%20");
        } else if (s.toString().length() > 0) {
            bean = new ArrayList<>();
            url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + s.toString().trim() + "&location=" + lat + "," + lng + "&radius=50&sensor=true&key=" + browserKey;
            Log.e("url", url);
            paserdata parse = new paserdata();
            parse.execute();
        }
    }

    public class paserdata extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts

                    JSONArray contacts = json.getJSONArray("predictions");
                    sugestion = new ArrayList<String>();
                    bean = new ArrayList<>();
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        PlacesBean bn = new PlacesBean();
                        JSONArray ja = c.getJSONArray("terms");
                        if (ja.length() > 0) {
                            bn.setDescription(description);
                            bn.setPlace(ja.getJSONObject((0)).getString("value"));
                            bn.setCity(ja.getJSONObject((ja.length() - 3)).optString("value"));
                            bn.setState(ja.getJSONObject((ja.length() - 2)).optString("value"));
                            bn.setCountry(ja.getJSONObject((ja.length() - 1)).optString("value"));
                            bean.add(bn);
                        }
                        sugestion.add(description);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            //			pb.setVisibility(View.GONE);
            ListingAdapter adapter = new ListingAdapter(bean, ListAddress.this);
            listPlaces.setAdapter(adapter);
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//            pb.setVisibility(View.VISIBLE);
        }


    }

}
