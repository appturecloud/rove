package com.rovecab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.Dialog.DialogAddCard;
import com.fragments.CarSpecificationFragment;
import com.interfaces.DismissDialog;
import com.service.AsyncTaskListener;
import com.service.CustomEditText;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class add_payment extends AppCompatActivity implements AsyncTaskListener, DismissDialog {

    private Toolbar toolbar = null;
    private Button btnSave = null;
    private CustomEditText edtCard = null;
    private CustomEditText edtMonth = null;
    private CustomEditText edtYear = null;
    private CustomEditText edtCvv = null;
    private CustomEditText selectedEditText = null;
    private LinearLayout errorLayout = null;
    private CustomText errorText = null;
    private ImageView cardImage = null;

    private boolean monthChecked = false;
    private boolean yearSelected = false;
    private boolean request = false;
    private String strCardImage = null;
    private String custId = null;
    private String key = null;
    private String cardNumber = null;
    private String[] values = null;
    private int textLength = 0;
    private int DELAY = 5000;
    private int previousLength = 0;

    private Handler handler = null;
    private Context context = null;
    private AsyncTaskListener listener = null;
    private DismissDialog dismissDialog = null;
    private SharedPreferences mpref = null;
    private ProgressDialog progressDialog = null;
    private List<NameValuePair> nameValuePairs = null;
    private List<NameValuePair> param_request = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        edtCard = (CustomEditText) findViewById(R.id.card_num);
        edtCvv = (CustomEditText) findViewById(R.id.edt_cvv);
        edtMonth = (CustomEditText) findViewById(R.id.edt_month);
        edtYear = (CustomEditText) findViewById(R.id.edt_yr);
        errorText = (CustomText) findViewById(R.id.txt_error_msg);
        btnSave = (Button) findViewById(R.id.save);
        cardImage = (ImageView) findViewById(R.id.card_image);
        errorLayout = (LinearLayout) findViewById(R.id.error);

        handler = new Handler();
        context = add_payment.this;
        listener = add_payment.this;
        dismissDialog = add_payment.this;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            request = bundle.getBoolean("request");
            if (request) {
                param_request = CarSpecificationFragment.nameValuePairs;
            }
        }

        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Checking card details");

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        custId = mpref.getString("id", null);
        key = mpref.getString("key", null);

        edtMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedEditText = edtMonth;
                values = new String[12];
                for (int i = 0; i < 12; i++) {
                    if (i + 1 < 10) {
                        values[i] = "0" + String.valueOf(i + 1);
                    } else {
                        values[i] = String.valueOf(i + 1);
                    }
                }

                DialogAddCard card = new DialogAddCard(context, dismissDialog, values, "Select a Month", true);
                card.show();
            }
        });

        edtYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedEditText = edtYear;
                values = new String[20];
                for (int i = 0; i < 20; i++) {
                    values[i] = String.valueOf(i + 2016);
                }
                DialogAddCard card = new DialogAddCard(context, dismissDialog, values, "Select a Year", false);
                card.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (monthChecked && yearSelected) {
                    String cardno = edtCard.getText().toString().replace(" ", "");
                    if (cardno.length() > 15 && strCardImage != null) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        progressDialog.setMessage("checking card details.");
                        progressDialog.show();

                        nameValuePairs = new ArrayList<>();

                        nameValuePairs.add(new BasicNameValuePair("uId", custId));
                        nameValuePairs.add(new BasicNameValuePair("default", "0"));
                        nameValuePairs.add(new BasicNameValuePair("key", key));
                        nameValuePairs.add(new BasicNameValuePair("cardType", strCardImage));
                        nameValuePairs.add(new BasicNameValuePair("cardNo", edtCard.getText().toString().replace(" ", "")));
                        nameValuePairs.add(new BasicNameValuePair("expiryMonth", edtMonth.getText().toString()));
                        nameValuePairs.add(new BasicNameValuePair("expiryYear", edtYear.getText().toString()));
                        nameValuePairs.add(new BasicNameValuePair("cvv", edtCvv.getText().toString()));

                        new HttpAsync(context, listener, getString(R.string.BASE_URL) + "service/addpaymentmethod", nameValuePairs, 2, "add_payment").execute();
                    } else {
                        setSnackMessage("Invalid card");
                    }
                } else {
                    setSnackMessage("Invalid month/year.");
                }

//                Intent myint = new Intent(add_payment.this, Payment_listing.class);
//                startActivity(myint);
            }
        });


        edtCard.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (edtCard.getSelectionEnd() > 0) {
                        if ((edtCard.getSelectionEnd() % 5) == 0) {
                            edtCard.setSelection(edtCard.getSelectionEnd() - 1);
                        }
                    }
                }
                return false;
            }
        });
        edtCard.addTextChangedListener(new TextWatcher() {

            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cardNumber = s.toString().replace(" ", "");
                String cardExpression = null;
                if (cardNumber.length() > 1) {
                    cardExpression = s.toString().substring(0, 2).trim();
                }
                else{
                    cardExpression = s.toString().trim();
                }
                textLength = count;

                if (s.toString().trim().length() < 3 && s.toString().trim().length()>0) {
                    if (cardExpression.startsWith("34") || cardExpression.startsWith("37")) {  //done
                        strCardImage = "american express";
                        cardImage.setImageResource(R.drawable.cc_americanexpress);
                    } else if (cardExpression.startsWith("67")) {
                        strCardImage = "maestro";
                        cardImage.setImageResource(R.drawable.cc_maestro);
                    } else if (Integer.parseInt(cardExpression) >= 51 && Integer.parseInt(cardExpression) <= 55) { //done
                        strCardImage = "master card";
                        cardImage.setImageResource(R.drawable.cc_mastercard);
                    } else if (cardExpression.startsWith("4")) {    //done
                        strCardImage = "visa";
                        cardImage.setImageResource(R.drawable.cc_visa);
                    } else if (cardExpression.startsWith("60") || cardExpression.startsWith("65")) { //done
                        strCardImage = "discover";
                        cardImage.setImageResource(R.drawable.cc_discover);
                    } else {
                        strCardImage = null;
                        cardImage.setImageResource(R.drawable.cc_default);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                int pos = 0;
                while (true) {
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);
                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }


        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        Log.e("result", result);
        try {
            JSONObject jobj = new JSONObject(result);
            if (tag.equalsIgnoreCase("add_payment")) {
                if (jobj != null) {
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
                        SharedPreferences.Editor edt = mpref.edit();
                        edt.putBoolean("card", true);
                        edt.commit();
                        Toast.makeText(context, "Card successfully added", Toast.LENGTH_SHORT).show();
                        if (!request) {
                            finish();
                        } else if (request) {
                            progressDialog.setMessage("requesting ride.");
                            progressDialog.show();
                            new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/requestride", param_request, 2, "request-ride").execute();
                        } else {
                            progressDialog.dismiss();
                        }
                    } else {
                        setSnackMessage(jobj.getString("msg"));
                        progressDialog.dismiss();
                    }
                }
            } else if (tag.equalsIgnoreCase("request-ride")) {
                if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 11) {
                    progressDialog.dismiss();
                    Intent myint = new Intent(context, HomeActivity.class);
                    myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(myint);
                    ((Activity) context).finish();
                } else {
                    setSnackMessage(jobj.getString("msg"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setSnackMessage(String message) {
        errorText.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
                handler();
//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                        errorLayout.getWidth(), errorLayout.getHeight());
//                lp.setMargins(0, errorLayout.getWidth(), 0, 0);
//                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                errorLayout.setLayoutParams(lp);
            }
        });
//        expand(errorLayout);
    }


    public void handler() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation slide = null;
                slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                        0.0f, Animation.RELATIVE_TO_SELF, -1.5f);

                slide.setDuration(400);
                slide.setFillAfter(true);
                slide.setFillEnabled(true);
                errorLayout.startAnimation(slide);

                slide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        errorLayout.setVisibility(View.GONE);
                        errorLayout.clearAnimation();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, DELAY);
    }

    @Override
    public void dismiss(Intent myint) {
        if (myint.getExtras().getBoolean("month")) {
            monthChecked = true;
        } else {
            yearSelected = true;
        }
        selectedEditText.setText(myint.getExtras().getString("val"));
    }
}
