package com.rovecab;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adapter.ListingAdapter;
import com.adapter.NavItemsAdapter;
import com.bean.CardsBean;
import com.bean.PlacesBean;
import com.fragments.CustomMapView;
import com.fragments.FragmentListCars;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.service.AsyncTaskListener;
import com.service.CustomText;
import com.service.HttpAsync;
import com.service.SmoothProgressDrawable;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements DrawerLayout.DrawerListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, AsyncTaskListener, SlidingUpPanelLayout.PanelSlideListener {

    public static LinearLayout homeToolbar = null;
    public static ImageView imgLocationPointer = null;
    public static CustomText pickupLocation = null;
    public static CustomText dropLocation = null;
    public static LinearLayout myLocation = null;
    public static LinearLayout layoutTimngs = null;
    public static RelativeLayout slidinUpLayout = null;
    public static CustomText selectedLocation = null;
    public static LinearLayout locationBox = null;
    public static GoogleMap mGoogleMap;
    public static Double sourceLat;
    public static Double sourceLng;
    public static Double destLat;
    public static Double destLng;
    public static int DELAY = 500;
    public static String sAddress = null;
    public static String dAddress = null;
    public static String sCity = null;
    public static String dCity = null;
    public static String sCountry = null;
    public static String dCounty = null;
    public static String location_addr = null;
    public static boolean selectedList = false;
    public static String userId = null;
    public static String dbDate = null;
    public static String dbTime = null;
    public static String dateTime = null;
    public static String key = null;
    public static ProgressDialog progressDialog = null;
    public static LinearLayout layoutSLideUpPanel = null;
    //    public static CustomText appxTime = null;
//    public static CustomText appxDist = null;
    public static Context context = null;
    public static SlidingUpPanelLayout slidingLayout = null;
    //    private ListView listCars = null;
    public static FragmentManager fragmentManager = null;
    public static Handler handler = null;
    public static Runnable runnable;
    public static AsyncTaskListener listener = null;
    public static SimpleDateFormat simpleDateFormat = null;
    public static Calendar calendar = null;
    public static Animation fadeIn = null;
    public static Animation fadeOut = null;
    private static Animation shaking = null;
    private String url = null;
    private CustomMapView view;
    private ImageView imgLocationIndicator = null;
    private LinearLayout layoutLocationSelection = null;
    private EditText edtEditLocation = null;
    private ListView listLocation = null;
    private List<String> sugestion = null;
    private List<PlacesBean> bean = null;
    private boolean lction = false;
    private Animation bottomUp = null;
    private Animation bottomDown = null;
    private boolean isLayoutOpened = false;
    private ProgressBar progressBar = null;
    private String search_result = null;
    private ImageView imgSedan = null;
    private ImageView imgSuv = null;
    private ImageView imgLimo = null;
    //    private ViewPager pager = null;
    private ListView navItems = null;
    private Toolbar toolbar = null;
    private LinearLayout selectDate = null;
    private LinearLayout selectTime = null;
    private LinearLayout editAcc = null;
    private LinearLayout layoutPickup = null;
    private LinearLayout layoutDrop = null;
    private DrawerLayout drawerLayout = null;
    private LinearLayout errorLayout;
    private CustomText errorText;
    private CustomText bookDate = null;
    //    private CustomText bookMonth = null;
//    private CustomText bookYear = null;
    private CustomText bookTime = null;
    private CustomText txtEmail = null;
    private CustomText txtPhone = null;
    private LocationRequest mLocationRequest;
    private Location location = null;
    private AddressResult mReceiver = null;
    private Intent int1 = null;
    private SharedPreferences mpref = null;

    private boolean isDestination = true;
    private String name = null;
    private String phone = null;
    private String address = null;
    private String amPm = null;
    private int rideType = 0;
    private LatLng center;
    private android.support.v4.app.Fragment fragment = null;
    private Geocoder geocoder;
    private List<Address> addresses;
    private BroadcastReceiver mConnReceiver;
    private int calHour = 0;
    private int calMinute = 0;
    private int calYear = 0;
    private int calMonth = 0;
    private int calDate = 0;
    private String minutes, hour = null;
    private Calendar cal = null;
    private List<CardsBean> cardBean = null;
    private ArrayList<NameValuePair> nameValuePairs = null;
    private String BASE_URL = null;
    //    private SlideUpPagerAdapter pagerAdapter = null;
    private String[] strNavItems = {"Payments", "Cash Credits", "Ride History", "Help", "Terms"};
    private int[] navIcons = {R.drawable.drawer_payment, R.drawable.drawer_cash_credit, R.drawable.drawer_history, R.drawable.drawer_help, R.drawable.drawer_terms};

    @Override
    protected void onRestart() {
        super.onRestart();
//        startService(int1);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        imgSedan = (ImageView) findViewById(R.id.sedan);
        imgSuv = (ImageView) findViewById(R.id.suv);
        imgLimo = (ImageView) findViewById(R.id.limo);
        pickupLocation = (CustomText) findViewById(R.id.pick_up);
        dropLocation = (CustomText) findViewById(R.id.drop_location);
        myLocation = (LinearLayout) findViewById(R.id.my_location);
        errorText = (CustomText) findViewById(R.id.txt_error_msg);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        selectDate = (LinearLayout) findViewById(R.id.select_day);
        selectTime = (LinearLayout) findViewById(R.id.select_date);
        drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        slidinUpLayout = (RelativeLayout) findViewById(R.id.sliding_up_layout);
        navItems = (ListView) findViewById(R.id.list_item);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        locationBox = (LinearLayout) findViewById(R.id.location_box);
        homeToolbar = (LinearLayout) findViewById(R.id.home_toolbar);
        editAcc = (LinearLayout) findViewById(R.id.layout_edit_account);
        imgLocationPointer = (ImageView) findViewById(R.id.location_pointer);
        layoutPickup = (LinearLayout) findViewById(R.id.layout_pickup);
        layoutDrop = (LinearLayout) findViewById(R.id.layout_drop);
        bookDate = (CustomText) findViewById(R.id.booking_date);
        layoutSLideUpPanel = (LinearLayout) findViewById(R.id.fragments_cars);
        imgLocationIndicator = (ImageView) findViewById(R.id.location_indicator);
//        bookMonth = (CustomText) findViewById(R.id.booking_month);
//        bookYear = (CustomText) findViewById(R.id.booking_year);
        bookTime = (CustomText) findViewById(R.id.book_time);
        txtEmail = (CustomText) findViewById(R.id.txt_name);
        txtPhone = (CustomText) findViewById(R.id.txt_phone);
        layoutTimngs = (LinearLayout) findViewById(R.id.layout_timings);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        layoutLocationSelection = (LinearLayout) findViewById(R.id.layout_select_location);
        edtEditLocation = (EditText) findViewById(R.id.edt_place);
        listLocation = (ListView) findViewById(R.id.listing);
//        appxTime = (CustomText) findViewById(R.id.appx_timing);
//        appxDist = (CustomText) findViewById(R.id.appx_dist);

        view = (CustomMapView) findViewById(R.id.mapView);

        dAddress = null;

        context = HomeActivity.this;
        cal = Calendar.getInstance();
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mReceiver = new AddressResult(new Handler());
        location = new Location("gps");
        handler = new Handler();

        progressBar.setIndeterminateDrawable(new SmoothProgressDrawable.Builder(this).interpolator(new AccelerateInterpolator()).build());

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Fetching rides");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);

        fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(context, R.anim.fade_out);
        shaking = AnimationUtils.loadAnimation(context, R.anim.shaking);
        bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
        bottomDown = AnimationUtils.loadAnimation(context, R.anim.bottom_down);
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        listener = HomeActivity.this;
        location.setLatitude(Double.parseDouble(mpref.getString("lat", null)));
        location.setLongitude(Double.parseDouble(mpref.getString("lng", null)));
        name = mpref.getString("name", null);
        phone = mpref.getString("mobile", null);
        userId = mpref.getString("id", null);
        key = mpref.getString("key", null);
        txtPhone.setText(phone);
        txtEmail.setText(name);


        fragment = new FragmentListCars();
        slidingLayout.addPanelSlideListener(this);

        calendar = Calendar.getInstance();

        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + 20);

        calHour = cal.get(Calendar.HOUR);
        calMinute = cal.get(Calendar.MINUTE);
        calYear = cal.get(Calendar.YEAR);
        calMonth = cal.get(Calendar.MONTH);
        calDate = cal.get(Calendar.DAY_OF_MONTH);

        if (cal.get(Calendar.MINUTE) < 10) {
            minutes = "0" + cal.get(Calendar.MINUTE);
        } else {
            minutes = String.valueOf(cal.get(Calendar.MINUTE));
        }
        if (cal.get(Calendar.HOUR) == 0) {
            hour = "12";
        } else if (cal.get(Calendar.HOUR) < 10) {
            hour = "0" + cal.get(Calendar.HOUR);
        } else {
            hour = String.valueOf(cal.get(Calendar.HOUR));
        }

        if (cal.get(Calendar.AM_PM) == 1) {
            amPm = "PM";
            if (cal.get(Calendar.HOUR) == 0) {
                calHour = 12;
            }
            bookTime.setText(hour + ":" + minutes + " PM");
        } else {
            if (cal.get(Calendar.HOUR) == 0) {
                calHour = 12;
            }
            amPm = "AM";
            bookTime.setText(hour + ":" + minutes + " AM");
        }
        dbTime = hour + ":" + minutes + " " + amPm;
        String currentDate = cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
        simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");
        String month = null;
        String year = null;
        switch (cal.get(Calendar.MONTH)) {
            case 0:
                month = "Jan";
                break;
            case 1:
                month = "Feb";
                break;
            case 2:
                month = "Mar";
                break;
            case 3:
                month = "Apr";
                break;
            case 4:
                month = "May";
                break;
            case 5:
                month = "Jun";
                break;
            case 6:
                month = "Jul";
                break;
            case 7:
                month = "Aug";
                break;
            case 8:
                month = "Sep";
                break;
            case 9:
                month = "Oct";
                break;
            case 10:
                month = "Nov";
                break;
            case 11:
                month = "Dec";
                break;
        }
        year = String.valueOf(cal.get(Calendar.YEAR));
        year = year.substring(year.length() - 2, year.length());
        bookDate.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)) + ", " + month + " " + year);

        calendar.set(calYear, calMonth, calDate, calHour, calMinute, 0);

        dateTime = String.valueOf(calendar.getTimeInMillis());
        try {
            Date date = new Date();
            date.setDate(cal.get(Calendar.DAY_OF_MONTH) - 1);
            date.setMonth(cal.get(Calendar.MONTH));
            date.setYear(cal.get(Calendar.YEAR));
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE");
            String day = (String) android.text.format.DateFormat.format("EEEE", date);
            day = dateFormat.format(date);
            day = day.substring(0, 3);
            dbDate = day + ", " + cal.get(Calendar.DAY_OF_MONTH) + " " + month + " " + year;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        bookMonth.setText(new DateFormatSymbols().getMonths()[cal.get(Calendar.MONTH)]);
//        bookYear.setText(String.valueOf(cal.get(Calendar.YEAR)));


//        int1 = new Intent(HomeActivity.this, FetchAddressIntentService.class);
        mConnReceiver = new connectionCheck();
        registerReceiver(this.mConnReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        sourceLat = sourceLng = destLat = destLng = null;
        sourceLat = location.getLatitude();
        sourceLng = location.getLongitude();

        runnable = new Runnable() {
            @Override
            public void run() {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                slidingLayout.setShadowHeight(4);
                layoutTimngs.setVisibility(View.VISIBLE);
                slidinUpLayout.setVisibility(View.VISIBLE);
                myLocation.setVisibility(View.VISIBLE);
            }
        };
//        int1.putExtra("location", location);
//        int1.putExtra("receiver", mReceiver);

        new GetLocationAsync(location.getLatitude(), location.getLongitude()).execute();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(HomeActivity.this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.setDrawerListener(this);
        toggle.syncState();
        navItems = (ListView) findViewById(R.id.nav_list_item);
        final NavItemsAdapter adpt = new NavItemsAdapter(context, strNavItems, navIcons);
        navItems.setAdapter(adpt);

        slidingLayout.addPanelSlideListener(this);
        navItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawerLayout.closeDrawers();
                Intent myint = null;
                adpt.notifyDataSetChanged();
                switch (position) {
                    case 0:
                        progressDialog.setMessage("Checking card details");
                        progressDialog.show();
                        nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("uId", userId));
                        nameValuePairs.add(new BasicNameValuePair("key", key));

                        new HttpAsync(context, listener, getString(R.string.BASE_URL) + "service/mypaymentmodes", nameValuePairs, 2, "card").execute();
                        break;

                    case 1:
                        myint = new Intent(HomeActivity.this, rove_credits.class);
                        startActivity(myint);
                        break;

                    case 2:
                        myint = new Intent(HomeActivity.this, ActivityMyTrip.class);
                        myint.putExtra("home", true);
                        startActivity(myint);
                        break;

                    case 3:
                        myint = new Intent(HomeActivity.this, terms.class);
                        startActivity(myint);
                        break;

                    case 4:
                        myint = new Intent(HomeActivity.this, terms.class);
                        startActivity(myint);
                        break;
                }
            }
        });

        edtEditLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listAddress(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    isLayoutOpened = false;
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    location_addr = bean.get(position).getDescription();
                    Intent myint = new Intent();
                    myint.putExtra("location", location_addr);
                    myint.putExtra(getString(R.string.city), bean.get(position).getCity());
                    myint.putExtra(getString(R.string.country), bean.get(position).getCountry());
                    myint.putExtra("icon", lction);
                    HomeActivity act = new HomeActivity();
                    act.onActivityResult(100, 100, myint);
                    edtEditLocation.setText(null);
                    listLocation.setAdapter(null);
                    listLocation.setAnimation(bottomDown);
                    layoutLocationSelection.startAnimation(bottomDown);
                } catch (Exception e) {

                }
            }
        });


        bottomUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                layoutLocationSelection.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(layoutLocationSelection.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        bottomDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                layoutLocationSelection.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                layoutTimngs.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                layoutTimngs.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        shaking.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                dropLocation.setTextColor(Color.BLACK);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        layoutPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                selectedList = true;
                lction = true;
                isLayoutOpened = true;
                edtEditLocation.setHint("Enter pickup location");
                edtEditLocation.requestFocus();
                imgLocationIndicator.setImageResource(R.drawable.pickup_blue);
                layoutLocationSelection.setAnimation(bottomUp);
                layoutLocationSelection.startAnimation(bottomUp);
            }
        });


        layoutDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                selectedList = true;
                isDestination = true;
                lction = false;
                edtEditLocation.requestFocus();
                edtEditLocation.setHint("Enter drop location");
                isLayoutOpened = true;
                imgLocationIndicator.setImageResource(R.drawable.drop_red);
                layoutLocationSelection.setAnimation(bottomUp);
                layoutLocationSelection.startAnimation(bottomUp);
            }
        });

        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                Calendar now = Calendar.getInstance();
                Calendar now1 = Calendar.getInstance();
                now1.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH) + 60);
                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(HomeActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.setYearRange(2016, 2016);
                datePickerDialog.setMinDate(now);
                datePickerDialog.setMaxDate(now1);
                datePickerDialog.show(getFragmentManager(), "date Picker");
            }
        });

        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                Calendar now = Calendar.getInstance();
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(HomeActivity.this, now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE), false);
                timePickerDialog.show(getFragmentManager(), "time picker");
            }
        });

        editAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent(HomeActivity.this, edit_account.class);
                startActivity(myint);
                drawerLayout.closeDrawers();
            }
        });
        imgSedan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragment = fragmentManager.findFragmentById(R.id.frgament_sample);
                fragment = new FragmentListCars();
                rideType = 1;
                imgSedan.setImageResource(R.drawable.ic_cabtype_normal_sedan);
                imgSuv.setImageResource(R.drawable.ic_cabtype_deselect_suv);
                imgLimo.setImageResource(R.drawable.ic_cabtype_deselect_limo);
                Fragment f = fragmentManager.findFragmentById(R.id.fragments_cars);
                if (f != null) {
                    fragmentManager.beginTransaction().remove(f);
                }
                if (dAddress != null) {
                    getCarList(rideType);

                } else {
                    Toast.makeText(context, "Drop location required", Toast.LENGTH_SHORT).show();
                    dropLocation.setTextColor(Color.RED);
                    dropLocation.startAnimation(shaking);
                }
            }
        });

        imgSuv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new FragmentListCars();
                rideType = 2;
                imgSedan.setImageResource(R.drawable.ic_cabtype_deselect_sedan);
                imgSuv.setImageResource(R.drawable.ic_cabtype_normal_suv);
                imgLimo.setImageResource(R.drawable.ic_cabtype_deselect_limo);
                Fragment f = fragmentManager.findFragmentById(R.id.fragments_cars);
                if (f != null) {
                    fragmentManager.beginTransaction().remove(f);
                }
                if (dAddress != null) {
                    getCarList(rideType);
                } else {
                    Toast.makeText(context, "Drop location required", Toast.LENGTH_SHORT).show();
                    dropLocation.setTextColor(Color.RED);
                    dropLocation.startAnimation(shaking);
                }
            }
        });

        imgLimo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new FragmentListCars();
                rideType = 3;
                imgSedan.setImageResource(R.drawable.ic_cabtype_deselect_sedan);
                imgSuv.setImageResource(R.drawable.ic_cabtype_deselect_suv);
                imgLimo.setImageResource(R.drawable.ic_cabtype_normal_limo);
                Fragment f = fragmentManager.findFragmentById(R.id.fragments_cars);
                if (f != null) {
                    fragmentManager.beginTransaction().remove(f);
                }
                if (dAddress != null) {
                    getCarList(rideType);
                } else {
                    Toast.makeText(context, "Drop location required", Toast.LENGTH_SHORT).show();
                    dropLocation.setTextColor(Color.RED);
                    dropLocation.startAnimation(shaking);
//                    setSnackMessage("Please select drop location.");
                }
            }
        });


        view.onCreate(savedInstanceState);
        view.onResume();

        mGoogleMap = view.getMap();
        view.init(mGoogleMap);

        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            MapsInitializer.initialize(context);
        }

        stupMap(location.getLatitude(), location.getLongitude());


        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition arg0) {
                // TODO Auto-generated method stub
                center = mGoogleMap.getCameraPosition().target;
//                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                    markerLayout.setVisibility(View.VISIBLE);
//                if (isDestination) {
//                    new GetLocationAsync(center.latitude, center.longitude).execute();
//                }
            }
        });

        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });


        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sourceLng = location.getLongitude();
                sourceLat = location.getLatitude();
                new GetLocationAsync(location.getLatitude(), location.getLongitude()).execute(new String[0]);

                LatLng latLong = new LatLng(location.getLatitude(), location.getLongitude());

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(16f).build();
                mGoogleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        name = mpref.getString("fname", null) + " " + mpref.getString("lname", null);
        phone = mpref.getString("mobile", null);
        txtPhone.setText(phone);
        txtEmail.setText(name);
        registerReceiver(this.mConnReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100 && requestCode == 100) {
            Bundle bundle = data.getExtras();
            isDestination = bundle.getBoolean("icon");
            if (bundle.getBoolean("icon")) {
                selectedLocation = pickupLocation;
                sCity = bundle.getString("city");
                sAddress = bundle.getString("location");
                sCountry = bundle.getString("country");
//                imgLocationPointer.setImageResource(R.drawable.ic_location_marker);
            } else {
                dCity = bundle.getString("city");
                dAddress = bundle.getString("location");
                dCounty = bundle.getString("country");
                selectedLocation = dropLocation;
//                imgLocationPointer.setImageResource(R.drawable.ic_location_marker_red);
            }

            selectedLocation.setText(bundle.getString("location"));

            List<Address> address;
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List addressList = geocoder.getFromLocationName(bundle.getString("location"), 1);

                if (addressList != null && addressList.size() > 0) {
                    Address add = (Address) addressList.get(0);
                    if (bundle.getBoolean("icon")) {
                        sourceLat = Double.valueOf(add.getLatitude());
                        sourceLng = Double.valueOf(add.getLongitude());
                        stupMap(Double.valueOf(add.getLatitude()), Double.valueOf(add.getLongitude()));
//                        new GetLocationAsync(sourceLat, sourceLng).execute();
                        return;
                    } else {
                        destLat = Double.valueOf(add.getLatitude());
                        destLng = Double.valueOf(add.getLongitude());
//                        new GetLocationAsync(destLat, destLng).execute();
                        findDistance(sourceLat, sourceLng, destLat, destLng);
                    }
//                    Barcode.GeoPoint geoPoint = new Barcode.GeoPoint(1, add.getLatitude() * 1E6, add.getLongitude() * 1E6);
//                    stupMap(add.getLatitude(), add.getLongitude());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bundle.getBoolean("icon")) {

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isLayoutOpened) {
            isLayoutOpened = false;
            layoutLocationSelection.startAnimation(bottomDown);
            edtEditLocation.setText(null);
            listLocation.setAdapter(null);
        } else {
            if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED && fragmentManager.getBackStackEntryCount() > 1) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                fragmentManager.popBackStack();
            } else if (this.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                this.drawerLayout.closeDrawers();
            } else if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            } else {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        stopService(int1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mConnReceiver);
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        showItems();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
//        slidinUpLayout.setVisibility(View.VISIBLE);
        showItems();
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        showItems();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        minutes = String.valueOf(minute);
        hour = String.valueOf(hourOfDay);
        calHour = hourOfDay;
        calMinute = minute;


        String minutes = null;
        String hour = null;
        String ampm = null;
        if (minute < 10) {
            minutes = "0" + minute;
        } else {
            minutes = String.valueOf(minute);
        }

        if (hourOfDay > 12) {
            if (hourOfDay - 12 >= 10) {
                hour = String.valueOf(hourOfDay - 12);
                ampm = "PM";
                bookTime.setText((hourOfDay - 12) + ":" + minutes + " PM");
            } else {
                hour = "0" + (hourOfDay - 12);
                ampm = "PM";
                bookTime.setText("0" + (hourOfDay - 12) + ":" + minutes + " PM");
            }
        } else {
            if (hourOfDay >= 10) {
                hour = String.valueOf(hourOfDay);
                ampm = "AM";
                bookTime.setText((hourOfDay) + ":" + minutes + " AM");
            } else {
                hour = "0" + hourOfDay;
                ampm = "AM";
                bookTime.setText("0" + (hourOfDay) + ":" + minutes + " AM");
            }
        }

        dbTime = hour + ":" + minutes + " " + ampm;

        calendar.set(calYear, calMonth, calDate, calHour, calMinute, 0);
        dateTime = String.valueOf(calendar.getTimeInMillis());

//        if (hourOfDay < 10 && hourOfDay <=12) {
//            hour = "0" + hourOfDay + " AM";
//        } else {
//            hour = hourOfDay + " PM";
//        }
//        bookTime.setText(hour + ":" + minutes);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        calYear = year;
        calMonth = monthOfYear;
        calDate = dayOfMonth;

        String month = null;
        String year1 = null;
        switch (cal.get(Calendar.MONTH)) {
            case 0:
                month = "Jan";
                break;
            case 1:
                month = "Feb";
                break;
            case 2:
                month = "Mar";
                break;
            case 3:
                month = "Apr";
                break;
            case 4:
                month = "May";
                break;
            case 5:
                month = "Jun";
                break;
            case 6:
                month = "Jul";
                break;
            case 7:
                month = "Aug";
                break;
            case 8:
                month = "Sep";
                break;
            case 9:
                month = "Oct";
                break;
            case 10:
                month = "Nov";
                break;
            case 11:
                month = "Dec";
                break;
        }
        year1 = String.valueOf(year);
        year1 = year1.substring(year1.length() - 2, year1.length());
        String currentDate = dayOfMonth + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + year1;
        try {
            Date date = simpleDateFormat.parse(currentDate);
            date.setDate(dayOfMonth);
            date.setMonth((cal.get(Calendar.MONTH) + 1));
            date.setYear(year);
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE");
            Date d = new Date();
            d.setDate(dayOfMonth - 1);
            d.setMonth(monthOfYear);
            d.setYear(year);
            String day = dateFormat.format(d);
            day = day.substring(0, 3);
            dbDate = day + ", " + dayOfMonth + " " + month + " " + year1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        bookDate.setText(dayOfMonth + ", " + month + " " + year1);
        calendar.set(calYear, calMonth, calDate, calHour, calMinute, 0);

        dateTime = String.valueOf(calendar.getTimeInMillis());
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        JSONObject jobj = null;
        try {
            jobj = new JSONObject(result);
            if (jobj != null) {
                if (tag.equalsIgnoreCase("direction")) {
                    JSONArray jarr = jobj.optJSONArray("routes");
                    JSONObject routes = jarr.optJSONObject(0);
                    try {

                        JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                        String encodedString = overviewPolylines.getString("points");
                        List<LatLng> list = decodePoly(encodedString);
                        mGoogleMap.clear();
                        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(destLat.doubleValue(), destLng.doubleValue())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker_red)));
                        //noinspection ResourceType
                        Polyline line = mGoogleMap.addPolyline(new PolylineOptions()
                                .addAll(list)
                                .width(12f).color(getResources().getInteger(R.color.colorAccent))
                                .geodesic(true));


                    } catch (Exception e) {
                        setSnackMessage("Service not available for selected Pickup & Drop");
                    }

                } else if (tag.equals("search_rides")) {
//                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    progressBar.setVisibility(View.INVISIBLE);
                    if (jobj.getString("status").equalsIgnoreCase("success")) {
                        this.search_result = result;
                        fragment = new FragmentListCars();
                        Bundle bundle = new Bundle();
                        bundle.putString("result", search_result);
                        bundle.putInt("type", rideType);
                        fragment.setArguments(bundle);
                        if (fragmentManager.getBackStackEntryCount() > 0) {
                            fragmentManager.popBackStack();
//                            fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
                        }
                        fragmentManager.beginTransaction().replace(R.id.fragments_cars, fragment).addToBackStack("cabs").commit();

                    } else if (jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 110) {
                        this.search_result = result;
                        fragment = new FragmentListCars();
                        Bundle bundle = new Bundle();
                        bundle.putString("result", search_result);
                        bundle.putInt("type", rideType);
                        fragment.setArguments(bundle);
                        if (fragmentManager.getBackStackEntryCount() > 0) {
                            fragmentManager.popBackStack();
//                            fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
                        }
                        fragmentManager.beginTransaction().replace(R.id.fragments_cars, fragment).addToBackStack("cabs").commit();
                    } else {
                        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        setSnackMessage("Could not fetch cabs.");
                    }
                } else if (tag.equalsIgnoreCase("card")) {
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
                        progressDialog.dismiss();
                        int selected = 0;
                        JSONArray jarr = jobj.optJSONArray("cards");
                        if (jarr != null && jarr.length() > 0) {
                            cardBean = new ArrayList<>();
                            for (int i = 0; i < jarr.length(); i++) {
                                JSONObject jo = jarr.getJSONObject(i);
                                CardsBean bean = new CardsBean();
                                bean.setCardId(jo.getInt("card"));
                                bean.setCardNo(jo.getString("cardNo"));
                                bean.setCardYype(jo.getString("type"));
                                bean.setSelected(jo.getInt("default"));
                                if (jo.getInt("default") == 1) {
                                    selected = i;
                                }
                                cardBean.add(bean);
                            }
                            Intent myint = new Intent(HomeActivity.this, Payment_listing.class);
                            myint.putExtra("cards", (Serializable) cardBean);
                            myint.putExtra("selected", selected);
                            startActivity(myint);
                        } else if (jobj.optString("cards") != null) {
                            Intent myint = new Intent(HomeActivity.this, payment_empty.class);
                            startActivity(myint);
                        }
                    }
                } else if (tag.equalsIgnoreCase("address")) {
                    findDistance(sourceLat, sourceLng, destLat, destLng);
                    if (jobj.getString("status").equalsIgnoreCase("OK")) {
                        JSONArray jarr = jobj.getJSONArray("results");
                        if (jarr.length() > 0) {
                            JSONObject jo = jarr.getJSONObject(0);
                            pickupLocation.setText(jo.getString("formatted_address"));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
        if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
            if (fragmentManager.getBackStackEntryCount() > 0) {
//                            fragmentManager.popBackStack();
                HomeActivity.fragmentManager.beginTransaction().remove(HomeActivity.fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
            }
        }
    }

    private void stupMap(Double lat, Double lng) {
        try {
            LatLng latLong = null;
            // TODO Auto-generated method stub

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            // Enabling MyLocation in Google Map
            mGoogleMap.setMyLocationEnabled(false);

            mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);

//            mGoogleMap.setPadding(0, 0, 0, 200);
//            if (mLocationClient.getLastLocation() != null) {
            latLong = new LatLng(lat, lng);

//                ShopLat = mLocationClient.getLastLocation().getLatitude() + "";
//                ShopLong = mLocationClient.getLastLocation().getLongitude()
//                        + "";
//
//            }
//            mGoogleMap.getUiSettings().setIndoorLevelPickerEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(16f).build();
            mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

//            new GetLocationAsync(lat, lng).execute(new String[0]);

//            mGoogleMap.addMarker(new MarkerOptions().position(latLong).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

//            mGoogleMap.setPadding(0, 200, 0, 0);
//
//            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLong));
//            VisibleRegion visibleRegion = mGoogleMap.getProjection().getVisibleRegion();
//            LatLngBounds mapLatLngBound = visibleRegion.latLngBounds;

//            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mapLatLngBound.getCenter()));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void showItems() {

//            handle();
        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(200);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
//            slidinUpLayout.startAnimation(slide);
        if (slidinUpLayout.getVisibility() == View.GONE) {
            slidinUpLayout.startAnimation(slide);
            layoutTimngs.startAnimation(fadeIn);
        }


        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                slidinUpLayout.setVisibility(View.VISIBLE);
                slidingLayout.setShadowHeight(4);
                slidinUpLayout.setVisibility(View.VISIBLE);
                myLocation.setVisibility(View.VISIBLE);
            }
        });

        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    public void handle() {
        handler.postDelayed(runnable, DELAY);
    }

    public void hideItems() {

        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
//            HomeActivity.fragmentManager.beginTransaction().remove(HomeActivity.fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
        }

        slidingLayout.setShadowHeight(0);
//        layoutTimngs.setVisibility(View.GONE);
        slidinUpLayout.setVisibility(View.GONE);
        myLocation.setVisibility(View.GONE);


        Animation slide = null;

        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 1.0f);


        slide.setDuration(200);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        slidinUpLayout.startAnimation(slide);
        layoutTimngs.startAnimation(fadeOut);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                slidinUpLayout.setVisibility(View.GONE);
                slidinUpLayout.clearAnimation();
            }
        });
    }

    public void findDistance(double sourceLat, double sourceLng, Double destLat, Double destLng) {

        if (destLat != null) {
//            new paserdata().execute(new String[]{distanceURL});
//            getCarList(0);
            ArrayList<NameValuePair> nameValuePair = new ArrayList<>();
            nameValuePair.add(new BasicNameValuePair("origin", sourceLat + "," + sourceLng));
            nameValuePair.add(new BasicNameValuePair("destination", String.valueOf(destLat) + "," + String.valueOf(destLng)));
            nameValuePair.add(new BasicNameValuePair("sensor", "false"));
            nameValuePair.add(new BasicNameValuePair("mode", "driving"));
            nameValuePair.add(new BasicNameValuePair("alternatives", "true"));
            nameValuePair.add(new BasicNameValuePair("key", "AIzaSyB97nemny-vWrJok57O1esdboxQtAQ8rb8"));
            new HttpAsync(context, listener, "https://maps.googleapis.com/maps/api/directions/json", nameValuePair, 2, "direction").execute();
        }
    }

    public void getLocation() {
        center = mGoogleMap.getCameraPosition().target;
        isDestination = true;
        try {
            sourceLng = Double.valueOf(HomeActivity.this.center.longitude);
            sourceLat = Double.valueOf(HomeActivity.this.center.latitude);
            new GetLocationAsync(center.latitude, center.longitude).execute(new String[0]);

        } catch (Exception e) {
        }
    }

    public void disableErrorMessage() {
        Animation slide = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, GroundOverlayOptions.NO_DIMENSION);
        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        this.errorLayout.startAnimation(slide);
        slide.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                HomeActivity.this.errorLayout.clearAnimation();
                HomeActivity.this.errorLayout.setVisibility(View.GONE);
            }
        });
    }

    public void setSnackMessage(String message) {
        this.errorText.setText(message);
        Animation slide = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, GroundOverlayOptions.NO_DIMENSION, 1, 0.0f);
        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        this.errorLayout.setVisibility(View.VISIBLE);
        this.errorLayout.startAnimation(slide);
        slide.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                HomeActivity.this.errorLayout.clearAnimation();
                handler1();
            }
        });
    }

    public void expandSlidingUp() {
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }

    public void getCarList(int rideType) {
        progressBar.setVisibility(View.VISIBLE);
        BASE_URL = context.getString(R.string.BASE_URL) + "service/searchrides";
//        BASE_URL = "http://appture.in/rove/search-rides";
        nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("pickup", sAddress));
        nameValuePairs.add(new BasicNameValuePair("drop", dAddress));
        nameValuePairs.add(new BasicNameValuePair("pickupLat", String.valueOf(sourceLat)));
        nameValuePairs.add(new BasicNameValuePair("pickupLon", String.valueOf(sourceLng)));
        nameValuePairs.add(new BasicNameValuePair("dropLat", String.valueOf(destLat)));
        nameValuePairs.add(new BasicNameValuePair("dropLon", String.valueOf(destLng)));
        nameValuePairs.add(new BasicNameValuePair("sCity", sCity));
        nameValuePairs.add(new BasicNameValuePair("sCountry", sCountry));
        nameValuePairs.add(new BasicNameValuePair("dCity", dCity));
        nameValuePairs.add(new BasicNameValuePair("dCountry", dCounty));
        nameValuePairs.add(new BasicNameValuePair("dbDate", dbDate));
        nameValuePairs.add(new BasicNameValuePair("dbTime", dbTime));
        nameValuePairs.add(new BasicNameValuePair("dateTime", dateTime));
        nameValuePairs.add(new BasicNameValuePair("type", String.valueOf(rideType)));
        nameValuePairs.add(new BasicNameValuePair("uId", userId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, BASE_URL, nameValuePairs, 2, "search_rides").execute();
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }

    public void handler1() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation slide = null;
                slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                        0.0f, Animation.RELATIVE_TO_SELF, -1.5f);

                slide.setDuration(400);
                slide.setFillAfter(true);
                slide.setFillEnabled(true);
                errorLayout.startAnimation(slide);

                slide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        errorLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, 2500);
    }

    public void convertToAddress(double lat, double lng) {
        //google api for converting lat and longitude to address
    }

    public void listAddress(String s) {

        if (s.toString().contains(" ")) {
            s = s.toString().replace(" ", "%20");
        } else if (s.toString().length() > 0) {
            bean = new ArrayList<>();
            url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + s.toString().trim() + "&location=" + location.getLatitude() + "," + location.getLongitude() + "&radius=50&sensor=true&key=AIzaSyDLkJPLf-aIdaCeRbTLgvLYMJuw5d_FJSo";
            paserdata parse = new paserdata();
            parse.execute();
        }
    }

    private class GetLocationAsync extends AsyncTask<String, Void, String> {

        // boolean duplicateResponse;
        double x, y;

        public GetLocationAsync(double latitude, double longitude) {
            // TODO Auto-generated constructor stub

            x = latitude;
            y = longitude;
        }

        @Override
        protected String doInBackground(String... params) {

            geocoder = new Geocoder(context, Locale.ENGLISH);
            try {
                addresses = geocoder.getFromLocation(x, y, 5);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (geocoder.isPresent()) {

                if (addresses != null) {
                    try {
                        Address returnAddress = addresses.get(0);

                        String localityString = returnAddress.getLocality();
                        String city = returnAddress.getCountryName();
                        String region_code = returnAddress.getCountryCode();
                        String zipcode = returnAddress.getPostalCode();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else {
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (!isDestination) {
//                    dropLocation.setText(addresses.get(1).getAddressLine(0) + ", "
//                            + addresses.get(0).getAddressLine(1) + " ");
//                    dCity = addresses.get(0).getLocality();
//                    dCounty = addresses.get(0).getCountryName();
//                    dAddress = addresses.get(0).getAddressLine(0) + ", "
//                            + addresses.get(0).getAddressLine(1);

                    findDistance(sourceLat.doubleValue(), sourceLng.doubleValue(), x, y);
                } else {
                    new HttpAsync(context, listener, "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + x + "," + y + "&sensor=true", new ArrayList<NameValuePair>(), 2, "address").execute();
//                    pickupLocation.setText(addresses.get(0).getAddressLine(0) + ", "
//                            + addresses.get(0).getAddressLine(1) + " ");

                    sCity = addresses.get(0).getLocality();
                    sCountry = addresses.get(0).getCountryName();
                    sAddress = addresses.get(0).getAddressLine(0) + ", "
                            + addresses.get(0).getAddressLine(1);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    @SuppressLint("ParcelCreator")
    class AddressResult extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public AddressResult(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            address = resultData.getString("Address");
            sCity = resultData.getString("city");
            sCountry = resultData.getString("country");
            pickupLocation.setText(address);
            sAddress = address + " " + sCountry;


        }
    }

    class connectionCheck extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra("noConnectivity", false);
            String reason = intent.getStringExtra("reason");
            boolean isFailover = intent.getBooleanExtra("isFailover", false);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra("otherNetwork");
            if (((NetworkInfo) intent.getParcelableExtra("networkInfo")).isConnected()) {
                HomeActivity.this.disableErrorMessage();
            } else {
                HomeActivity.this.setSnackMessage("Check your internet connection");
            }
        }
    }

    public class paserdata extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            JSONObject json = jParser.getJSONFromUrl(url.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts

                    JSONArray contacts = json.getJSONArray("predictions");
                    sugestion = new ArrayList<String>();
                    bean = new ArrayList<>();
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        PlacesBean bn = new PlacesBean();
                        JSONArray ja = c.getJSONArray("terms");
                        if (ja.length() > 0) {
                            bn.setDescription(description);
                            bn.setPlace(ja.getJSONObject((0)).getString("value"));
                            bn.setCity(ja.getJSONObject((ja.length() - 3)).optString("value"));
                            bn.setState(ja.getJSONObject((ja.length() - 2)).optString("value"));
                            bn.setCountry(ja.getJSONObject((ja.length() - 1)).optString("value"));
                            bean.add(bn);
                        }
                        sugestion.add(description);
                    }
                } catch (JSONException e) {
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            //			pb.setVisibility(View.GONE);
            ListingAdapter adapter = new ListingAdapter(bean, context);
            listLocation.setAdapter(adapter);
        }


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//            pb.setVisibility(View.VISIBLE);
        }


    }

}
