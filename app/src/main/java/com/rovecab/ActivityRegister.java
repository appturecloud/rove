package com.rovecab;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Dialog.SelectCountryDialog;
import com.WebResponse.APIRegister;
import com.bean.CountryBean;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.DismissDialog;
import com.interfaces.WebServiceController;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import java.util.ArrayList;
import java.util.regex.Pattern;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sujith on 16-05-2016.
 */
public class ActivityRegister extends AppCompatActivity implements DismissDialog {

    private Toolbar toolbar = null;
    private TextView txtReferral = null;
    private TextInputLayout referral = null;
    private LinearLayout errorLayout = null;
    private RelativeLayout layout = null;

    private CustomEditText email = null;
    private CustomEditText password = null;
    private CustomEditText conPass = null;
    private CustomEditText first_name = null;
    private CustomEditText last_name = null;
    private CustomEditText referal = null;
    private CustomEditText mobile = null;
    private CustomEditText edtFlag = null;
    private CustomText txtErrorMsg = null;
    private CustomButton btnRegister = null;

    private WebServiceController webRequestController = null;
    private Retrofit retrofit = null;
    private ProgressDialog progressDialog = null;
    private SharedPreferences mpref = null;
    private Handler handler = null;

    private Pattern emailPattern = null;
    private Account[] accounts = null;
    private String possibleEmail = null;
    private String country_id = null;
    private ArrayList<CountryBean> countryBean = null;
    private String countryName = null;
    private int DELAY = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        layout = (RelativeLayout) findViewById(R.id.layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);

        emailPattern = Patterns.EMAIL_ADDRESS;

        countryBean = (ArrayList<CountryBean>) getIntent().getSerializableExtra("countries");
        countryName = mpref.getString("cntry", null);

        handler = new Handler();

        progressDialog = new ProgressDialog(ActivityRegister.this);
        progressDialog.setCancelable(false);
        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();
        webRequestController = retrofit.create(WebServiceController.class);


        txtReferral = (TextView) findViewById(R.id.txt_referral);
        referral = (TextInputLayout) findViewById(R.id.referral);
        email = (CustomEditText) findViewById(R.id.email);
        password = (CustomEditText) findViewById(R.id.password);
        conPass = (CustomEditText) findViewById(R.id.con_password);
        first_name = (CustomEditText) findViewById(R.id.first_name);
        last_name = (CustomEditText) findViewById(R.id.last_name);
        referal = (CustomEditText) findViewById(R.id.referal_code);
        mobile = (CustomEditText) findViewById(R.id.mobile);
        btnRegister = (CustomButton) findViewById(R.id.register);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txtErrorMsg = (CustomText) findViewById(R.id.txt_error_msg);
        edtFlag = (CustomEditText) findViewById(R.id.edt_flag);

        for (int i = 0; i < countryBean.size(); i++) {
            if (countryBean.get(i).getName().equalsIgnoreCase(countryName)) {
                String uri = "@drawable/" + countryBean.get(i).getFlag();
                int imgRes = getResources().getIdentifier(uri, null, getPackageName());
                Drawable img = getResources().getDrawable(imgRes);
                img.setBounds(0, 0, 10, 10);
                edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                edtFlag.setText("+" + countryBean.get(i).getCountry_code());
                country_id = countryBean.get(i).getCountry_id();
                break;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ActivityRegister.this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(ActivityRegister.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_PHONE_STATE}, 1000);
            } else {
                getAccounts();
            }
        } else {
            getAccounts();
        }

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (password.getText().toString().length() < 6) {
                        setSnackMessage("Invalid password");
                    }
                }
            }
        });

        conPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (password.getText().toString().length() < 1) {
                        setSnackMessage("Password Cannot be Empty.");
                    }
                    if (!hasFocus && password.getText().toString().equalsIgnoreCase(conPass.getText().toString())) {
                        setSnackMessage("Password and confirm password does not matching.");
                    }
                }
            }
        });

        conPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == password.getText().length()) {
                    if (!s.toString().equals(password.getText().toString())) {
                        setSnackMessage("Password and confirm password does not matching.");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txtReferral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referral.setVisibility(View.VISIBLE);
                txtReferral.setVisibility(View.GONE);
            }
        });

        edtFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCountryDialog selectCountryDialog = new SelectCountryDialog(ActivityRegister.this, countryBean, ActivityRegister.this);
                selectCountryDialog.show();
            }
        });

        mobile.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    if (checkInternetConnection()) {
                        if (email.getText().toString().length() > 0 && emailPattern.matcher(email.getText().toString()).matches()) {
                            if (password.getText().toString().length() >= 6 && password.getText().toString().equalsIgnoreCase(conPass.getText().toString())) {
                                if (first_name.getText().toString().length() > 0) {
                                    if (last_name.getText().toString().length() > 0) {
                                        if (mobile.getText().toString().length() > 0) {
                                            progressDialog.setMessage("Please wait while Registering");
                                            progressDialog.show();
                                            Call<APIRegister> result = webRequestController.register(country_id, first_name.getText().toString(), last_name.getText().toString(), email.getText().toString(),
                                                    password.getText().toString(), mobile.getText().toString(), "1", referal.getText().toString());

                                            result.enqueue(new Callback<APIRegister>() {
                                                @Override
                                                public void onResponse(Response<APIRegister> response, Retrofit retrofit) {

                                                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                                                        progressDialog.dismiss();
                                                        SharedPreferences.Editor editor = mpref.edit();
                                                        editor.putString("mobile", mobile.getText().toString());
                                                        editor.putString("fname", first_name.getText().toString());
                                                        editor.putString("lname", last_name.getText().toString());
                                                        editor.putString("email", email.getText().toString());
                                                        editor.putString("id", response.body().getId());
                                                        editor.putString("key", response.body().getKey());
                                                        editor.commit();

                                                        Intent myint = new Intent(ActivityRegister.this, ActivityVerification.class);
                                                        myint.putExtra("mobile", mobile.getText().toString());
                                                        myint.putExtra("otp", response.body().getOtp());
                                                        myint.putExtra("id", response.body().getId());
                                                        myint.putExtra("key", response.body().getKey());
                                                        myint.putExtra("forgot", false);
                                                        startActivity(myint);
                                                    } else {
                                                        progressDialog.dismiss();
                                                        setSnackMessage(response.body().getMessage());
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Throwable t) {

                                                }
                                            });
                                        } else {
                                            setSnackMessage("Phone number could not be empty");
                                        }
                                    } else {
                                        setSnackMessage("Last name could not be empty");
                                    }
                                } else {
                                    setSnackMessage("First name could not be empty");
                                }
                            } else {
                                setSnackMessage("Check the password and confirm password");
                            }
                        } else {
                            setSnackMessage("Invalid email");
                        }
                    } else {
                        setSnackMessage("Check Internet Connection");
                    }
                    return true;
                }
                return false;
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkInternetConnection()) {
                    if (email.getText().toString().length() > 0 && emailPattern.matcher(email.getText().toString()).matches()) {
                        if (password.getText().toString().length() >= 6 && password.getText().toString().equalsIgnoreCase(conPass.getText().toString())) {
                            if (first_name.getText().toString().length() > 0) {
                                if (last_name.getText().toString().length() > 0) {
                                    if (mobile.getText().toString().length() > 0) {
                                        progressDialog.setMessage("Please wait while Registering");
                                        progressDialog.show();
                                        Call<APIRegister> result = webRequestController.register(country_id, first_name.getText().toString(), last_name.getText().toString(), email.getText().toString(),
                                                password.getText().toString(), mobile.getText().toString(), "1", referal.getText().toString());

                                        result.enqueue(new Callback<APIRegister>() {
                                            @Override
                                            public void onResponse(Response<APIRegister> response, Retrofit retrofit) {

                                                if (response.body().getStatus().equalsIgnoreCase("success")) {
                                                    progressDialog.dismiss();
                                                    SharedPreferences.Editor editor = mpref.edit();
                                                    editor.putString("mobile", mobile.getText().toString());
                                                    editor.putString("fname", first_name.getText().toString());
                                                    editor.putString("lname", last_name.getText().toString());
                                                    editor.putString("email", email.getText().toString());
                                                    editor.putString("id", response.body().getId());
                                                    editor.putString("key", response.body().getKey());
                                                    editor.commit();

                                                    Intent myint = new Intent(ActivityRegister.this, ActivityVerification.class);
                                                    myint.putExtra("mobile", mobile.getText().toString());
                                                    myint.putExtra("otp", response.body().getOtp());
                                                    myint.putExtra("id", response.body().getId());
                                                    myint.putExtra("key", response.body().getKey());
                                                    myint.putExtra("forgot", false);
                                                    startActivity(myint);
                                                } else {
                                                    progressDialog.dismiss();
                                                    setSnackMessage(response.body().getMessage());
                                                }
                                            }

                                            @Override
                                            public void onFailure(Throwable t) {

                                            }
                                        });
                                    } else {
                                        setSnackMessage("Phone number could not be empty");
                                    }
                                } else {
                                    setSnackMessage("Last name could not be empty");
                                }
                            } else {
                                setSnackMessage("First name could not be empty");
                            }
                        } else {
                            setSnackMessage("Check the password and confirm password");
                        }
                    } else {
                        setSnackMessage("Invalid email");
                    }
                } else {
                    setSnackMessage("Check Internet Connection");
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getAccounts();
        }
    }

    public void setSnackMessage(String message) {
        txtErrorMsg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
                handler();
//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                        errorLayout.getWidth(), errorLayout.getHeight());
//                lp.setMargins(0, errorLayout.getWidth(), 0, 0);
//                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                errorLayout.setLayoutParams(lp);
            }
        });
//        expand(errorLayout);
    }

    public boolean checkInternetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

//    public static void expand(final View v) {
//        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        final int targetHeight = v.getMeasuredHeight();
//
//        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
//        v.getLayoutParams().height = 1;
//        v.setVisibility(View.VISIBLE);
//        Animation a = new Animation() {
//            @Override
//            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                v.getLayoutParams().height = interpolatedTime == 1
//                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
//                        : (int) (targetHeight * interpolatedTime);
//                v.requestLayout();
//            }
//
//            @Override
//            public boolean willChangeBounds() {
//                return true;
//            }
//        };
//
//        // 1dp/ms
//        a.setDuration(350);
//        v.startAnimation(a);
//    }


    public void handler() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation slide = null;
                slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                        0.0f, Animation.RELATIVE_TO_SELF, -1.0f);

                slide.setDuration(400);
                slide.setFillAfter(true);
                slide.setFillEnabled(true);
                errorLayout.startAnimation(slide);

                slide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        errorLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, DELAY);
    }


    public void getAccounts() {
        accounts = AccountManager.get(ActivityRegister.this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;
                email.setText(possibleEmail);
                email.setSelectAllOnFocus(true);
                break;
            }
        }
    }

    @Override
    public void dismiss(Intent myint) {
        edtFlag.setText(myint.getStringExtra(getString(R.string.country)));
        String uri = "@drawable/" + myint.getStringExtra(getString(R.string.flag));
        int imgRes = getResources().getIdentifier(uri, null, getPackageName());
        Drawable img = ActivityRegister.this.getResources().getDrawable(imgRes);
        img.setBounds(0, 0, 10, 10);
        country_id = myint.getStringExtra("country_id");
        edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
