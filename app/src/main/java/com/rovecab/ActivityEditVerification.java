package com.rovecab;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.WebResponse.APIMobileVerification;
import com.WebResponse.APIResendOTP;
import com.WebResponse.APISaveProfile;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.WebServiceController;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sujith on 7/22/2016.
 */
public class ActivityEditVerification extends AppCompatActivity {

    public static Retrofit retrofit = null;
    public static WebServiceController webServiceController = null;
    private CustomEditText edtOtp = null;
    private CustomButton btnSubmit = null;
    private LinearLayout errorLayout = null;
    private CustomText txtErrorMsg = null;
    private LinearLayout autoRead = null;
    private LinearLayout layoutOtp = null;
    private LinearLayout resendOtp = null;
    private Handler handler = null;
    private Context context = null;
    private Runnable runnable = null;
    private SharedPreferences mpref = null;
    private ProgressDialog progressDialog = null;
    private BroadcastReceiver smsReceiver = null;
    private String mobile = null;
    private String id = null;
    private String key = null;
    private String str_otp = null;
    private String fname = null;
    private String lname = null;
    private String email = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        context = ActivityEditVerification.this;

        resendOtp = (LinearLayout) findViewById(R.id.btn_resend);
        edtOtp = (CustomEditText) findViewById(R.id.otp);
        btnSubmit = (CustomButton) findViewById(R.id.btn_verify);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txtErrorMsg = (CustomText) findViewById(R.id.txt_error_msg);
        autoRead = (LinearLayout) findViewById(R.id.enter_otp);
        layoutOtp = (LinearLayout) findViewById(R.id.layout_otp);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                autoRead.setVisibility(View.GONE);
                layoutOtp.setVisibility(View.VISIBLE);
            }
        };

        handler.postDelayed(runnable, 15000);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(999);


        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // code to read the incoming SMS
                Bundle bundle = intent.getExtras();
                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;

                    if (senderNum.startsWith("DM-02")) {
                        String message = currentMessage.getDisplayMessageBody();
                        str_otp = message.substring(message.length() - 5, message.length());
                        callAPI(true);
                        handler.removeCallbacks(runnable);
                    }
                }
            }
        };

        registerReceiver(smsReceiver, intentFilter);


        Bundle bundle = getIntent().getExtras();
        mobile = bundle.getString("mobile");
        id = bundle.getString("id");
        key = bundle.getString("key");
        fname = bundle.getString("fname");
        lname = bundle.getString("lname");
        email = bundle.getString("mail");
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);

        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();

        webServiceController = retrofit.create(WebServiceController.class);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOtp.getText().toString().length() > 0) {
                    progressDialog.setMessage("verifying OTP");
                    progressDialog.show();
                    callAPI(false);
                } else {
                    setSnackMessage("Enter the OTP.");
                }
            }
        });


        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Resending OTP");
                progressDialog.show();
                Call<APIResendOTP> resendOTP = webServiceController.generateOTP(id, key);
                resendOTP.enqueue(new Callback<APIResendOTP>() {
                    @Override
                    public void onResponse(Response<APIResendOTP> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            if (response.body().getStatus().equalsIgnoreCase("success")) {
                                str_otp = response.body().getOtp();
                                autoRead.setVisibility(View.VISIBLE);
                                layoutOtp.setVisibility(View.GONE);
                                handler.postDelayed(runnable, 10000);
                                progressDialog.dismiss();
                            } else {
                                setSnackMessage(response.body().getStatus());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(smsReceiver);
        } catch (IllegalArgumentException i) {

        }
    }

    public void setSnackMessage(String message) {
        txtErrorMsg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
            }
        });
    }

    public void callAPI(boolean autoRead) {
        String otp = null;

        if (!autoRead) {
            otp = edtOtp.getText().toString();
        } else if (autoRead) {
            otp = str_otp;
        }

        Call<APIMobileVerification> mobileVerification = webServiceController.verify(id, key, otp);
        mobileVerification.enqueue(new Callback<APIMobileVerification>() {
            @Override
            public void onResponse(Response<APIMobileVerification> response, Retrofit retrofit) {
                progressDialog.dismiss();
                if (response.body().getStatus().equalsIgnoreCase("success")) {

                    SharedPreferences.Editor edt = mpref.edit();
                    edt.putBoolean("login", true);
                    edt.putString("mobile", mobile);
                    edt.commit();

                    saveProfile();
                } else {
                    setSnackMessage(response.body().getMsg());
                }
            }


            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public void saveProfile() {
        progressDialog.setMessage("Updating user details");
        progressDialog.show();
        Call<APISaveProfile> saveProfileCall = webServiceController.saveProfile(id, key, fname, lname, email, mobile);

        saveProfileCall.enqueue(new Callback<APISaveProfile>() {
            @Override
            public void onResponse(Response<APISaveProfile> response, Retrofit retrofit) {
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    SharedPreferences.Editor edt = mpref.edit();
                    edt.putString("fname", fname);
                    edt.putString("lname", lname);
                    edt.putString("email", email);
                    edt.commit();
                    progressDialog.dismiss();
                    Toast.makeText(ActivityEditVerification.this, "Your Profile has been updated successfully", Toast.LENGTH_LONG).show();
                    finish();

                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

