package com.rovecab;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.Dialog.DialogLogout;
import com.WebResponse.APISaveProfile;
import com.bean.CountryBean;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.WebServiceController;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class edit_account extends AppCompatActivity implements AsyncTaskListener{

    private RelativeLayout changePassword = null;
    private Toolbar toolbar = null;

    private CustomEditText txtfName = null;
    private CustomEditText txtlName = null;
    private CustomEditText txtMail = null;
    private CustomEditText txtPhone = null;
    private CustomText txt_error_msg = null;
    private CustomButton btnSave = null;
    private LinearLayout errorLayout = null;

    private Retrofit retrofit = null;
    private Context context = null;
    private SharedPreferences mpref = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;
    private WebServiceController webRequestController = null;
    private List<NameValuePair> nameValuePairs = null;

    private String userId = null;
    private String key = null;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String fname = null;
    private String lname = null;
    private String mail = null;
    private String phone = null;
    private ArrayList<CountryBean> bean = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressDialog = new ProgressDialog(edit_account.this);
        progressDialog.setCancelable(false);

        txtfName = (CustomEditText) findViewById(R.id.first_name);
        txtlName = (CustomEditText) findViewById(R.id.last_name);
        txtMail = (CustomEditText) findViewById(R.id.mail);
        txtPhone = (CustomEditText) findViewById(R.id.mobile);
        btnSave = (CustomButton) findViewById(R.id.btn_save);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txt_error_msg = (CustomText) findViewById(R.id.txt_error_msg);
        changePassword = (RelativeLayout) findViewById(R.id.change_password);

        context = edit_account.this;
        listener = edit_account.this;

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        if (mpref != null) {
            fname = mpref.getString("fname", null);
            lname = mpref.getString("lname", null);
            mail = mpref.getString("email", null);
            phone = mpref.getString("mobile", null);
            txtfName.setText(fname);
            txtlName.setText(lname);
            txtMail.setText(mail);
            txtPhone.setText(phone);
            userId = mpref.getString("id", null);
            key = mpref.getString("key", null);
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();
        webRequestController = retrofit.create(WebServiceController.class);

        txtfName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtfName.getText().toString().equals(fname) && txtlName.getText().toString().equalsIgnoreCase(lname) && txtMail.getText().toString().equals(mail) && txtPhone.getText().toString().equals(phone)) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtfName.getText().toString().equals(fname) && txtlName.getText().toString().equalsIgnoreCase(lname) && txtMail.getText().toString().equals(mail) && txtPhone.getText().toString().equals(phone)) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtfName.getText().toString().equalsIgnoreCase(fname) && txtMail.getText().toString().equalsIgnoreCase(mail) && txtlName.getText().toString().equalsIgnoreCase(lname) && txtPhone.getText().toString().equals(phone)) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtlName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txtfName.getText().toString().equalsIgnoreCase(fname) && txtMail.getText().toString().equalsIgnoreCase(mail) && txtlName.getText().toString().equalsIgnoreCase(lname) && txtPhone.getText().toString().equals(phone)) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtMail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == EditorInfo.IME_ACTION_DONE){
                    callApi();
                    return true;
                }
                return false;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        if (mpref != null) {
            fname = mpref.getString("fname", null);
            lname = mpref.getString("lname", null);
            mail = mpref.getString("email", null);
            phone = mpref.getString("mobile", null);
            txtfName.setText(fname);
            txtlName.setText(lname);
            txtMail.setText(mail);
            txtPhone.setText(phone);
            userId = mpref.getString("id", null);
            key = mpref.getString("key", null);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.logout:
                DialogLogout logout = new DialogLogout(context);
                logout.show();
                break;
        }
        return true;
    }

    public void setSnackMessage(String message) {
        txt_error_msg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
            }
        });
    }

    public void callApi(){
        if(phone.equalsIgnoreCase(txtPhone.getText().toString())){
            if(txtPhone.getText().toString().length() >8){
                if (txtfName.getText().toString().length() > 0 && txtlName.getText().toString().length() > 0 && txtMail.getText().toString().length() > 0 && txtPhone.getText().toString().length() > 0){
                    if(txtMail.getText().toString().matches(emailPattern)){
                        progressDialog.setMessage("Updating your details");
                        progressDialog.show();
                        Call<APISaveProfile> saveProfileCall = webRequestController.saveProfile(userId, key, txtfName.getText().toString(), txtlName.getText().toString(), txtMail.getText().toString(), txtPhone.getText().toString());

                        saveProfileCall.enqueue(new Callback<APISaveProfile>() {
                            @Override
                            public void onResponse(Response<APISaveProfile> response, Retrofit retrofit) {
                                if(response.body().getStatus().equalsIgnoreCase("success")) {
                                    SharedPreferences.Editor edt = mpref.edit();
                                    edt.putString("fname", txtfName.getText().toString());
                                    edt.putString("lname", txtlName.getText().toString());
                                    edt.putString("email", txtMail.getText().toString());
                                    edt.commit();
                                    fname = txtfName.getText().toString();
                                    lname = txtlName.getText().toString();
                                    mail = txtMail.getText().toString();
                                    btnSave.setEnabled(false);
                                    txtfName.clearFocus();
                                    txtlName.clearFocus();
                                    txtMail.clearFocus();
                                    txtPhone.clearFocus();
                                    Toast.makeText(edit_account.this, "Your Profile has been updated successfully", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();

                                }
                                else{
                                    setSnackMessage(response.message());
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {

                            }
                        });

                    }else{
                        setSnackMessage("Invalid EmailId.");
                    }
                }
                else{
                    setSnackMessage("All fields are required.");
                }
            }
            else{
                setSnackMessage("Invalid Phone number");
            }
        }
        else {

            // call getOtp
            // mobile number is changed
            nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("uId", userId));
            nameValuePairs.add(new BasicNameValuePair("key", key));
            nameValuePairs.add(new BasicNameValuePair("phone", txtPhone.getText().toString()));
            progressDialog.setMessage("Checking phone number");
            progressDialog.show();
            new HttpAsync(context, listener, getString(R.string.BASE_URL) +"service/getotp", nameValuePairs, 2, "generate_otp").execute();
        }
    }


    public void changePassword(View view) {
        Intent myint = new Intent(edit_account.this, change_password.class);
        myint.putExtra("id", userId);
        myint.putExtra("key", key);
        myint.putExtra("change", true);
        startActivity(myint);
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if(jobj!=null){
                if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
                    progressDialog.dismiss();
                    Intent myint = new Intent(edit_account.this, ActivityEditVerification.class);
                    myint.putExtra("mobile", txtPhone.getText().toString());
                    myint.putExtra("id", mpref.getString("id", null));
                    myint.putExtra("key", mpref.getString("key", null));
                    myint.putExtra("fname", txtfName.getText().toString());
                    myint.putExtra("lname", txtlName.getText().toString());
                    myint.putExtra("mail", txtMail.getText().toString());
                    startActivity(myint);
                }
                else if(jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 31){
                    progressDialog.dismiss();
                    setSnackMessage("Mobile number already registered");
                }
                else{
                    setSnackMessage(jobj.getString("msg"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
