package com.rovecab;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WebResponse.APIChangePassword;
import com.bean.CountryBean;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.WebServiceController;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by VipiN Negi on 05/11/16.
 */
public class change_password extends AppCompatActivity {

    private Toolbar toolbar = null;
    private CustomEditText edtNewPass = null;
    private CustomEditText edtconPass = null;
    private CustomEditText edtOldPass = null;
    private CustomButton savePassword = null;
    private TextInputLayout inputOldPassword = null;
    private LinearLayout errorLayout = null;
    private CustomText txtError = null;

    private boolean changePassword = false;
    private String userId = null;
    private String key = null;
    private ArrayList<CountryBean> bean = null;

    private Retrofit retrofit = null;
    private WebServiceController webRequestController = null;
    private SharedPreferences mpref = null;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        edtNewPass = (CustomEditText) findViewById(R.id.new_password);
        edtconPass = (CustomEditText) findViewById(R.id.con_password);
        edtOldPass = (CustomEditText) findViewById(R.id.old_pass);
        savePassword = (CustomButton) findViewById(R.id.save_password);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txtError = (CustomText) findViewById(R.id.txt_error_msg);

        inputOldPassword = (TextInputLayout) findViewById(R.id.inputlayout_old_password);
        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        bean = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        try {
            InputStream inputStream = getAssets().open("phone.txt");
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String str;
            while ((str = buffer.readLine()) != null) {
                builder.append(str);
            }

            inputStream.close();

            JSONObject jobj = new JSONObject(builder.toString());
            if (jobj != null) {
                JSONArray jarr = jobj.getJSONArray("countries");
                if (jarr.length() > 0) {
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject jsonObject = jarr.getJSONObject(i);
                        CountryBean bean1 = new CountryBean();
                        bean1.setName(jsonObject.getString("country_name"));
                        bean1.setCountry_code(jsonObject.getString("country_code"));
                        bean1.setFlag(jsonObject.getString("country_flag"));
                        bean.add(bean1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progressDialog = new ProgressDialog(change_password.this);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            changePassword = bundle.getBoolean("change");
            userId = bundle.getString("id");
            key = bundle.getString("key");
            if (changePassword) {
                inputOldPassword.setVisibility(View.VISIBLE);
            }
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();
        webRequestController = retrofit.create(WebServiceController.class);

        edtconPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if (edtNewPass.getText().toString().length() > 5) {
                        if (edtNewPass.getText().toString().equalsIgnoreCase(edtconPass.getText().toString())) {
                            progressDialog.setMessage("Updating password");
                            progressDialog.show();
                            Call<APIChangePassword> reset = webRequestController.changePassword(userId, key, edtOldPass.getText().toString(), edtNewPass.getText().toString());

                            reset.enqueue(new Callback<APIChangePassword>() {
                                @Override
                                public void onResponse(Response<APIChangePassword> response, Retrofit retrofit) {
                                    progressDialog.dismiss();
                                    if (response.body().getStatus().equalsIgnoreCase("success")) {

                                        Toast.makeText(change_password.this, "Password successfully changed.", Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        progressDialog.dismiss();
                                        setSnackMessage(response.body().getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {

                                }
                            });
                        } else {
                            setSnackMessage("new password and confirm password doesn't matches.");
                        }
                    } else {
                        setSnackMessage("Invalid password.");
                    }
                    return true;
                }
                return false;
            }
        });

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNewPass.getText().toString().length() > 5) {
                    if (edtNewPass.getText().toString().equalsIgnoreCase(edtconPass.getText().toString())) {
                        progressDialog.setMessage("Updating password");
                        progressDialog.show();
                        Call<APIChangePassword> reset = webRequestController.changePassword(userId, key, edtOldPass.getText().toString(), edtNewPass.getText().toString());

                        reset.enqueue(new Callback<APIChangePassword>() {
                            @Override
                            public void onResponse(Response<APIChangePassword> response, Retrofit retrofit) {
                                progressDialog.dismiss();
                                if (response.body().getStatus().equalsIgnoreCase("success")) {

                                    Toast.makeText(change_password.this, "Password successfully changed.", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    setSnackMessage(response.body().getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {

                            }
                        });
                    } else {
                        setSnackMessage("new password and confirm password doesn't matches.");
                    }
                } else {
                    setSnackMessage("Invalid password.");
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public void setSnackMessage(String message) {
        txtError.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();

//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                        errorLayout.getWidth(), errorLayout.getHeight());
//                lp.setMargins(0, errorLayout.getWidth(), 0, 0);
//                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                errorLayout.setLayoutParams(lp);
            }
        });
//        expand(errorLayout);
    }
}
