package com.rovecab;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Dialog.SelectCountryDialog;
import com.WebResponse.APIForgotPassword;
import com.bean.CountryBean;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.DismissDialog;
import com.interfaces.WebServiceController;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sujith on 24-05-2016.
 */
public class ActivityForgotPassword extends AppCompatActivity implements DismissDialog {

    private CustomButton btnConfirm = null;
    private CustomEditText phoneNum = null;
    private CustomText txt_error_msg = null;
    private CustomEditText edtFlag = null;
    private LinearLayout errorLayout = null;
    private Toolbar toolbar = null;

    private Retrofit retrofit = null;
    private WebServiceController webServiceController = null;
    private ProgressDialog progressDialog = null;
    private ArrayList<CountryBean> bean = null;
    private String country = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        btnConfirm = (CustomButton) findViewById(R.id.btn_cofirm);
        phoneNum = (CustomEditText) findViewById(R.id.mobile_number);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txt_error_msg = (CustomText) findViewById(R.id.txt_error_msg);
        edtFlag = (CustomEditText) findViewById(R.id.edt_flag);
        progressDialog = new ProgressDialog(ActivityForgotPassword.this);
        progressDialog.setCancelable(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            country = bundle.getString("country");
        }

        bean = new ArrayList<CountryBean>();
        StringBuilder builder = new StringBuilder();
        try {
            InputStream inputStream = getAssets().open("phone.txt");
            BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String str;
            while ((str = buffer.readLine()) != null) {
                builder.append(str);
            }

            inputStream.close();

            JSONObject jobj = new JSONObject(builder.toString());
            if (jobj != null) {
                JSONArray jarr = jobj.getJSONArray("countries");
                if (jarr.length() > 0) {
                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject jsonObject = jarr.getJSONObject(i);
                        CountryBean bean1 = new CountryBean();
                        bean1.setName(jsonObject.getString("country_name"));
                        bean1.setCountry_code(jsonObject.getString("country_code"));
                        bean1.setFlag(jsonObject.getString("country_flag"));
                        bean.add(bean1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for (int i = 0; i < bean.size(); i++) {
            if (bean.get(i).getName().equalsIgnoreCase(country)) {
                String uri = "@drawable/" + bean.get(i).getFlag();
                int imgRes = getResources().getIdentifier(uri, null, getPackageName());
                Drawable img = ActivityForgotPassword.this.getResources().getDrawable(imgRes);
                img.setBounds(0, 0, 10, 10);
                edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                edtFlag.setText("+" + bean.get(i).getCountry_code());
                break;
            }
        }


        edtFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCountryDialog countryDialog = new SelectCountryDialog(ActivityForgotPassword.this, bean, ActivityForgotPassword.this);
                countryDialog.show();
            }
        });


        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();
        webServiceController = retrofit.create(WebServiceController.class);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPhone();
            }
        });

        phoneNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchPhone();
                    return true;
                }
                return false;
            }
        });
    }


    public void setSnackMessage(String message) {
        txt_error_msg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
            }
        });
    }

    @Override
    public void dismiss(Intent myint) {
        edtFlag.setText(myint.getStringExtra(getString(R.string.country)));
        String uri = "@drawable/" + myint.getStringExtra(getString(R.string.flag));
        int imgRes = getResources().getIdentifier(uri, null, getPackageName());
        Drawable img = ActivityForgotPassword.this.getResources().getDrawable(imgRes);
        img.setBounds(0, 0, 10, 10);
        edtFlag.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public void searchPhone() {

        if(phoneNum.getText().toString().length()>0){
            progressDialog.setMessage("Checking phone number");
            progressDialog.setIndeterminate(false);
            progressDialog.show();
            Call<APIForgotPassword> forgotPass = webServiceController.forgotPassword("98", phoneNum.getText().toString());

            forgotPass.enqueue(new Callback<APIForgotPassword>() {
                @Override
                public void onResponse(Response<APIForgotPassword> response, Retrofit retrofit) {

                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        progressDialog.dismiss();
                        Intent myint = new Intent(ActivityForgotPassword.this, ActivityMobileVerification.class);
                        myint.putExtra("mobile", phoneNum.getText().toString());
                        myint.putExtra("token", response.body().getResetToken());
                        myint.putExtra("forgot", true);
                        startActivity(myint);
                    }
                    else{
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

        }
        else{
            setSnackMessage("Please enter your phone number.");
        }
    }
}
