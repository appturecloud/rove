package com.rovecab;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.adapter.AdapterPaymentMethods;
import com.bean.CardsBean;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Payment_listing extends AppCompatActivity implements AsyncTaskListener{


    private int previousSelected = 0;
    private Toolbar toolbar = null;
    private LinearLayout errorLayout = null;
    private CustomText errorText = null;
    private ListView listCards = null;
//    public static CustomButton saveCards = null;

    private Handler handler = null;
    public static List<CardsBean> cardsBean = null;
    private Context context = null;
    public static AdapterPaymentMethods payments = null;
    private AsyncTaskListener listener = null;
    private SharedPreferences mpref = null;
    private ProgressDialog progressDialog = null;
    private List<NameValuePair> nameValuePairs = null;

    private String key = null;
    private String uId = null;
    public static String defaultCard = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_listing);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        previousSelected = getIntent().getIntExtra("selected", 0);
        cardsBean = (List<CardsBean>) getIntent().getExtras().getSerializable("cards");

        errorText = (CustomText) findViewById(R.id.txt_error_msg);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        listCards = (ListView) findViewById(R.id.cards_list);

        context = Payment_listing.this;
        listener = Payment_listing.this;

        handler = new Handler();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage("Updating your card.");

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        if(mpref!=null){
            uId = mpref.getString("id", null);
            key = mpref.getString("key", null);
        }

        payments = new AdapterPaymentMethods(Payment_listing.this, cardsBean, uId, key);
        listCards.setAdapter(payments);

        listCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cardsBean.get(previousSelected).setSelected(0);
                cardsBean.get(position).setSelected(1);
                previousSelected = position;
                payments.notifyDataSetChanged();
                progressDialog.setMessage("Updating your card.");
                progressDialog.show();
                nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("uId", uId));
                nameValuePairs.add(new BasicNameValuePair("key", key));
                nameValuePairs.add(new BasicNameValuePair("card", String.valueOf(cardsBean.get(position).getCardId())));

                new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/setdefaultcard", nameValuePairs, 2, "update_card").execute();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        progressDialog.setMessage("Fetching card details");
        progressDialog.show();
        nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("uId", uId));
        nameValuePairs.add(new BasicNameValuePair("key", key));

        new HttpAsync(context, listener, getString(R.string.BASE_URL)+ "service/mypaymentmodes", nameValuePairs, 2, "card").execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_payment_listing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.add_card:
                Intent myint = new Intent(context, add_payment.class);
                startActivity(myint);
                break;
        }
        return true;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if(jobj!=null){
                if(tag.equalsIgnoreCase("update_card")){
                    if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
                        progressDialog.dismiss();
                    }
                    else{
                        setSnackMessage(jobj.getString("msg"));
                    }
                }
                else if(tag.equalsIgnoreCase("card")){
                    progressDialog.dismiss();
                    int selected = 0;
                    JSONArray jarr = jobj.optJSONArray("cards");
                    if(jarr !=null && jarr.length()>0){
                        cardsBean = new ArrayList<>();
                        for(int i=0; i<jarr.length(); i++){
                            JSONObject jo = jarr.getJSONObject(i);
                            CardsBean bean = new CardsBean();
                            bean.setCardId(jo.getInt("card"));
                            bean.setCardNo(jo.getString("cardNo"));
                            bean.setCardYype(jo.getString("type"));
                            bean.setSelected(jo.getInt("default"));
                            if(jo.getInt("default") ==1){
                                selected = i;
                            }
                            cardsBean.add(bean);
                        }
                        payments = new AdapterPaymentMethods(Payment_listing.this, cardsBean, uId, key);
                        listCards.setAdapter(payments);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setEnabled(int cardNo){
        defaultCard = String.valueOf(cardNo);
    }

    public void setSnackMessage(String message) {
        this.errorText.setText(message);
        Animation slide = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, GroundOverlayOptions.NO_DIMENSION, 1, 0.0f);
        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        this.errorLayout.setVisibility(View.VISIBLE);
        this.errorLayout.startAnimation(slide);
        slide.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();
                handler1();
            }
        });
    }

    public void handler1() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation slide = null;
                slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                        Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                        0.0f, Animation.RELATIVE_TO_SELF, -1.5f);

                slide.setDuration(400);
                slide.setFillAfter(true);
                slide.setFillEnabled(true);
                errorLayout.startAnimation(slide);

                slide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        errorLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, 4000);
    }

    public void updateList(List<CardsBean> bn){
        cardsBean = new ArrayList<>();
        cardsBean = bn;
        payments.notifyDataSetChanged();
    }
}
