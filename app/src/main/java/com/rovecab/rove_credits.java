package com.rovecab;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class rove_credits extends AppCompatActivity implements AsyncTaskListener{

    private Toolbar toolbar = null;
    private TextView myCredits = null;
    private TextView currencySign = null;
    private TextView currencyCode = null;
    private CustomEditText edtFriendsEmail = null;
    private CustomButton sendRequest = null;
    private CustomText referalCode = null;
    private CustomText txtFriends = null;
    private LinearLayout progress = null;
    private LinearLayout contentMain = null;


    private Pattern emailPattern = null;
    private Context context = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;
    private ArrayList<NameValuePair> nameValuePairs = null;
    private SharedPreferences mpref = null;

    private String userId = null;
    private String key = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_credits);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        referalCode = (CustomText) findViewById(R.id.referal_code);
        myCredits = (TextView) findViewById(R.id.my_credits);
        currencySign = (TextView) findViewById(R.id.curr_sign);
        currencyCode = (TextView) findViewById(R.id.curr_code);
        edtFriendsEmail = (CustomEditText) findViewById(R.id.edt_friends_email);
        sendRequest = (CustomButton) findViewById(R.id.send_request);
        progress = (LinearLayout) findViewById(R.id.progressBar);
        contentMain = (LinearLayout) findViewById(R.id.content_main);
        txtFriends = (CustomText) findViewById(R.id.txt_friends);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        context = rove_credits.this;
        listener = rove_credits.this;
        userId = mpref.getString("id", null);
        key = mpref.getString("key", null);
        emailPattern = Patterns.EMAIL_ADDRESS;

        progressDialog = new ProgressDialog(context);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        edtFriendsEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().length() > 0 && emailPattern.matcher(s.toString()).matches()){
                    sendRequest.setEnabled(true);
                }
                else{
                    sendRequest.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtFriendsEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    progressDialog.setMessage("Sending invitation");
                    progressDialog.show();
                    nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("email", edtFriendsEmail.getText().toString()));
                    nameValuePairs.add(new BasicNameValuePair("key", key));
                    nameValuePairs.add(new BasicNameValuePair("uId", userId));
                    new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/referfriend", nameValuePairs, 2, "invitation").execute();
                    return true;
                }
                return false;
            }
        });
        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Sending invitation");
                progressDialog.show();
                nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", edtFriendsEmail.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("key", key));
                nameValuePairs.add(new BasicNameValuePair("uId", userId));
                new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/referfriend", nameValuePairs, 2, "invitation").execute();
            }
        });

        nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("uId", userId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/cashcredits", nameValuePairs, 2, "credits").execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if(tag.equalsIgnoreCase("credits")){
                if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
                    progressDialog.dismiss();
                    referalCode.setText(jobj.getString("referralCode"));
                    txtFriends.setText("You have invited "+ jobj.getInt("referrals")+" friend(s).");
                    if(!jobj.getString("currencySign").equalsIgnoreCase("")){
                        int hex = Integer.parseInt(jobj.getString("currencySign"), 16);
                        currencySign.setText((char)hex+"");
                        myCredits.setText(String.valueOf(jobj.getInt("credits")));
                    }
                    else{
                        myCredits.setText(jobj.getInt("credits"));
                        currencyCode.setText(jobj.getString("currencyCode"));
                    }
                    progress.setVisibility(View.GONE);
                    contentMain.setVisibility(View.VISIBLE);
                }
            }
            else if(tag.equalsIgnoreCase("invitation")){
                if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
                    Toast.makeText(context, "Request has been sent", Toast.LENGTH_LONG).show();
                    sendRequest.setEnabled(false);
                    edtFriendsEmail.setText(null);
                    progressDialog.dismiss();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
