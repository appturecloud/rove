package com.rovecab;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.Dialog.DialogCancelTrip;
import com.Dialog.DialogMyReview;
import com.Dialog.DialogNotes;
import com.Dialog.DialogUserComment;
import com.WebResponse.APITripsList;
import com.interfaces.DismissDialog;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomText;
import com.service.HttpAsync;
import com.service.ProgressDialog;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sujith on 23-05-2016.
 */
public class ActivityTripsDetails extends AppCompatActivity implements AsyncTaskListener, DismissDialog{

    private Toolbar toolbar = null;

    private CustomText tripId = null;
    private CustomText driverName = null;
    private CustomText cabName = null;
    private CustomText cabType = null;
    private CustomText tripStartTime = null;
    private CustomText tripEndTime = null;
    private CustomText pickUpAddress = null;
    private CustomText dropAddress = null;
    private CustomText baseFare = null;
//    private CustomText addOns = null;
//    private CustomText txtNote = null;
    private CustomText txtTip = null;
    private CustomText tip = null;
    private CustomText totlaAmount = null;
    private CustomText cardNo = null;
    private CustomText card_used = null;
    private CustomText cardx = null;
    private CustomText serviceChargePer = null;
    private CustomText txtServiceCharge = null;
    private ImageView notes = null;
    private CustomButton review = null;
    private CustomButton emailReceipt = null;
    private RelativeLayout parentLayout = null;
    private RelativeLayout parentLayout1 = null;
    private LinearLayout parentLayout2 = null;
    private LinearLayout callUser = null;
    private MLRoundedImageView driverImage = null;
    private LinearLayout progress = null;

    private APITripsList tripList = null;
    private ArrayList<NameValuePair> nameValuePairs = null;
    private SharedPreferences mpref = null;
    private Context context = null;
    private DismissDialog dismissDialog = null;
    private AsyncTaskListener listener = null;
    private String userReview = null;
    private String key = null;
    private int userRatings = 0;
    private String mobile = null;

    private String strNotes = null;
    private String uId = null;
    private int hex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        tripId = (CustomText) findViewById(R.id.trip_id);
        review = (CustomButton) findViewById(R.id.review);
        driverName = (CustomText) findViewById(R.id.driver_name);
        cabName = (CustomText) findViewById(R.id.cab_name);
        cabType = (CustomText) findViewById(R.id.cab_type);
        tripStartTime = (CustomText) findViewById(R.id.trip_started_time);
        tripEndTime = (CustomText) findViewById(R.id.trip_end_time);
        pickUpAddress = (CustomText) findViewById(R.id.pick_up_address);
        dropAddress = (CustomText) findViewById(R.id.drop_address);
        baseFare = (CustomText) findViewById(R.id.base_fare);
        txtTip = (CustomText) findViewById(R.id.tip);
        notes = (ImageView) findViewById(R.id.notes);
        callUser = (LinearLayout) findViewById(R.id.call_user);
//        txtNote = (CustomText) findViewById(R.id.txt_note);
//        addOns = (CustomText) findViewById(R.id.add_ons);
        tip = (CustomText) findViewById(R.id.tip_fare);
        totlaAmount = (CustomText) findViewById(R.id.total_amount);
        cardNo = (CustomText) findViewById(R.id.cardno);
        emailReceipt = (CustomButton) findViewById(R.id.reciept);
        driverImage = (MLRoundedImageView) findViewById(R.id.driver_image);
        card_used = (CustomText) findViewById(R.id.card_used);
        cardx = (CustomText) findViewById(R.id.cardx);
        parentLayout = (RelativeLayout) findViewById(R.id.parent_main);
        parentLayout1 = (RelativeLayout) findViewById(R.id.parent_main1);
        parentLayout2 = (LinearLayout) findViewById(R.id.parent_main2);
        progress = (LinearLayout) findViewById(R.id.progressBar);
        serviceChargePer = (CustomText) findViewById(R.id.service_chare_per);
        txtServiceCharge = (CustomText) findViewById(R.id.txt_service_charge);
        tripList = (APITripsList) getIntent().getSerializableExtra("details");

        tripId.setText(tripList.getTripId());
        cabName.setText(tripList.getCarBrand()+" "+ tripList.getCarModel());
        cabType.setText("(" + tripList.getCarType() +")");
        hex = Integer.parseInt(tripList.getCurrencySign(), 16);
        totlaAmount.setText((char)hex+"" + tripList.getTotalFare()+"("+tripList.getCurrencyCode()+")");

        if(tripList.getCardNo() == null || tripList.getCardNo().equalsIgnoreCase("")){
            cardNo.setText("Unpaid");
            card_used.setText("Payment status");
            cardx.setVisibility(View.GONE);
        }
        else{
            cardNo.setText(tripList.getCardNo());
        }

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);
        if(mpref!=null){
            uId = mpref.getString("id", null);
            key = mpref.getString("key", null);
        }

        context = ActivityTripsDetails.this;
        listener = ActivityTripsDetails.this;
        dismissDialog = ActivityTripsDetails.this;

        nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("trip", tripList.getTripId()));
        nameValuePairs.add(new BasicNameValuePair("uId", uId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/tripdetails", nameValuePairs, 2, "trip-details").execute();

        callUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mobile));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogNotes notes = new DialogNotes(context, strNotes);
                notes.show();
            }
        });
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(review.getText().toString().equalsIgnoreCase("REVIEW")){
                    DialogUserComment comment = new DialogUserComment(ActivityTripsDetails.this, dismissDialog, false, null, 0, tripList.getTripId(), uId, key);
                    comment.show();
                }
                else if(review.getText().toString().equalsIgnoreCase("MY REVIEW")){
                    DialogMyReview comment = new DialogMyReview(ActivityTripsDetails.this, true, userReview, userRatings, tripList.getTripId(), uId, key);
                    comment.show();
                }
                else if(review.getText().toString().equalsIgnoreCase("CANCEL")){
//                    progressDialog.show();
                    nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("trip", tripList.getTripId()));
                    nameValuePairs.add(new BasicNameValuePair("uId", uId));
                    nameValuePairs.add(new BasicNameValuePair("key", key));
                    new HttpAsync(context, listener, getString(R.string.BASE_URL)+"service/canceltrip", nameValuePairs, 2, "Cancel-trips").execute();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if(jobj!=null){
                if(tag.equalsIgnoreCase("Cancel-trips")){
                    if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
//                        progressDialog.dismiss();
                        DialogCancelTrip cancelTrip = new DialogCancelTrip(context, jobj.getString("fee"), jobj.getString("currency_sign"), jobj.getString("currency_code"), tripList.getTripId(), uId, key,dismissDialog);
                        cancelTrip.show();
                        cancelTrip.setCanceledOnTouchOutside(false);
                    }
                }
                else if(tag.equalsIgnoreCase("trip-details")){
                    if(jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1){
                        progress.setVisibility(View.GONE);
                        parentLayout.setVisibility(View.VISIBLE);
                        parentLayout1.setVisibility(View.VISIBLE);
                        parentLayout2.setVisibility(View.VISIBLE);
                        JSONObject jo = jobj.getJSONObject("trip");
                        driverName.setText(jo.getString("driverName"));
                        tripStartTime.setText(jo.getString("tripStarted"));
                        tripEndTime.setText(jo.getString("tripEnded"));
                        pickUpAddress.setText(jo.getString("pickupAddress"));
                        dropAddress.setText(jo.getString("dropAddress"));
                        baseFare.setText((char)hex +""+jo.getInt("fare"));
                        txtTip.setText("Tip ("+String.valueOf(jo.getInt("tipPercentage"))+"%)");
                        tip.setText((char)hex+"" + jo.getInt("tip"));
                        if(jo.getString("driverPhone").equalsIgnoreCase("")){
                            driverName.setVisibility(View.INVISIBLE);
                            callUser.setVisibility(View.GONE);
                            findViewById(R.id.driver_image).setVisibility(View.GONE);
                        }
                        else{
                            mobile = jo.getString("driverPhone");
                        }
                        userReview = jo.getString("userReview");
                        userRatings = jo.optInt("userRating");
                        serviceChargePer.setText("Service charge ("+jo.getInt("serviceChargePercentage")+"%)");
                        txtServiceCharge.setText((char)hex+"" +String.valueOf(jo.getString("serviceCharge")));
                        if(jo.getString("notes") != null && !jo.getString("notes").equalsIgnoreCase("null")){
                            strNotes = jo.getString("notes");
                            notes.setVisibility(View.VISIBLE);
                        }
                        else{
//                            txtNote.setVisibility(View.GONE);
                            notes.setVisibility(View.GONE);
                        }

                        UrlImageViewHelper.setUrlDrawable(driverImage, getString(R.string.BASE_URL)+"upload/drivers/profile-pic/"+ jo.getString("driverImage"));

                        if(jo!=null){
                            if(jo.getString("status").equalsIgnoreCase("Order Placed") || jo.getString("status").equalsIgnoreCase("Confirmed") || jo.getString("status").equalsIgnoreCase("Processing")){
                                review.setText("CANCEL");
                                emailReceipt.setEnabled(false);
                            }
                            else if(jo.getString("status").equalsIgnoreCase("Completed") && jo.getInt("review") == 1){
                                review.setText("REVIEW");
                                emailReceipt.setEnabled(true);
                            }
                            else if(jo.getString("status").equalsIgnoreCase("Completed") && jo.getInt("review") == 0){
                                review.setText("MY REVIEW");
                                emailReceipt.setEnabled(true);
                            }
                            else if(jo.getString("status").equalsIgnoreCase("Cancelled") || jo.getString("status").equalsIgnoreCase("Ongoing")){
                                review.setEnabled(false);
                                card_used.setText("Status");
                                cardx.setVisibility(View.GONE);
                                cardNo.setText("Canceled");
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss(Intent myint) {
        if(myint.getExtras().getBoolean("review") || myint.getExtras().getBoolean("isCancelled")){
            nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("trip", tripList.getTripId()));
            nameValuePairs.add(new BasicNameValuePair("uId", uId));
            nameValuePairs.add(new BasicNameValuePair("key", key));
            new HttpAsync(context, listener, context.getString(R.string.BASE_URL)+"service/tripdetails", nameValuePairs, 2, "trip-details").execute();
        }
    }
}
