package com.rovecab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.WebResponse.APIMobileVerification;
import com.WebResponse.APIOTPResend;
import com.WebResponse.APIResetPassword;
import com.github.aurae.retrofit.LoganSquareConverterFactory;
import com.interfaces.WebServiceController;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.CustomText;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sujith on 20-05-2016.
 */
public class ActivityMobileVerification extends AppCompatActivity {

    public static CustomEditText otp = null;
    public static CustomButton btnSubmit = null;
    public static LinearLayout setPasswordLayout = null;
    public static LinearLayout enterOtp = null;
    public static String str_otp = null;
    public static String str_mobile = null;
    public static String resetToken = null;
    public static String key = null;
    public static boolean forgotPass = false;
    public static Context context = null;
    public static SharedPreferences mpref = null;
    public static ProgressDialog progressDialog = null;
    public static Retrofit retrofit = null;
    public static WebServiceController webServiceController = null;
    public static Handler handler = null;
    public static Runnable runnable = null;
    private String userId = null;
    private CustomText txt_error_msg = null;
    private Toolbar toolbar = null;
    private LinearLayout resendOtp = null;
    private RelativeLayout layout = null;
    private LinearLayout layout_otp = null;
    private CustomEditText newPassword = null;
    private CustomEditText newPasswordManual = null;
    private LinearLayout errorLayout = null;
    private int i = 0;
    private String countryId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_verification);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        otp = (CustomEditText) findViewById(R.id.otp);
        resendOtp = (LinearLayout) findViewById(R.id.btn_resend);
        btnSubmit = (CustomButton) findViewById(R.id.btn_verify);
        layout = (RelativeLayout) findViewById(R.id.layout);
        setPasswordLayout = (LinearLayout) findViewById(R.id.setpasswordlayout);
        enterOtp = (LinearLayout) findViewById(R.id.enter_otp);
        newPassword = (CustomEditText) findViewById(R.id.new_password);
        layout_otp = (LinearLayout) findViewById(R.id.layout_otp);
        newPasswordManual = (CustomEditText) findViewById(R.id.new_password_manual);
        errorLayout = (LinearLayout) findViewById(R.id.error);
        txt_error_msg = (CustomText) findViewById(R.id.txt_error_msg);

        mpref = getSharedPreferences("user_details", MODE_PRIVATE);

        context = ActivityMobileVerification.this;
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                layout_otp.setVisibility(View.VISIBLE);
                enterOtp.setVisibility(View.GONE);
            }
        };

        handler.postDelayed(runnable, 15000);

        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.BASE_URL)).addConverterFactory(LoganSquareConverterFactory.create()).build();

        webServiceController = retrofit.create(WebServiceController.class);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            str_mobile = bundle.getString("mobile");
            forgotPass = bundle.getBoolean("forgot");
            resetToken = bundle.getString("token");
            key = bundle.getString("key");
            userId = bundle.getString("id");
        }
        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Resending OTP");
                progressDialog.show();
                Call<APIOTPResend> resendOTP = webServiceController.resendOTP(countryId, str_mobile);
                resendOTP.enqueue(new Callback<APIOTPResend>() {
                    @Override
                    public void onResponse(Response<APIOTPResend> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            if (response.body().getStatus().equalsIgnoreCase("success")) {
                                progressDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            }
        });

        otp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if (newPassword.getText().toString().length() >= 6) {
                        callApi(newPassword.getText().toString());
                    } else {
                        setSnackMessage("Invalid password.");
                    }
                    return true;
                }
                return false;
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = null;

                if (str_otp != null && str_otp.length() >= 5) {

                } else if (otp.getText().toString().length() > 0) {
                    str_otp = otp.getText().toString();
                } else {
                    setSnackMessage("Invalid otp");
                }
                if (forgotPass) {
                    if (newPassword.getText().toString().length() >= 6) {
                        callApi(newPassword.getText().toString());
                    } else {
                        setSnackMessage("Invalid password.");
                    }
                } else {
                    verifyUserId(str_otp);
                }
            }
        });

        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() >= 6) {
                    btnSubmit.setEnabled(true);
                } else {
                    btnSubmit.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void validateOTP(Context context1, String message) throws Exception {

        if (message.startsWith("Dear User, Your Verification code")) {
            message = message.substring(message.length() - 5, message.length());
            str_otp = message;

            enterOtp.setVisibility(View.GONE);
            if (forgotPass) {
                setPasswordLayout.setVisibility(View.VISIBLE);
            } else {
                verifyUserId(str_otp);
            }

            handler.removeCallbacks(runnable);
        }
    }

    public void callApi(String password){
        Call<APIResetPassword> resetPassword = webServiceController.resetPassword(password, resetToken, str_otp);
        resetPassword.enqueue(new Callback<APIResetPassword>() {
            @Override
            public void onResponse(Response<APIResetPassword> response, Retrofit retrofit) {
                if (response.body().getStatus().equalsIgnoreCase("success") && response.body().getCode() == 1) {
                    SharedPreferences.Editor editor = mpref.edit();
                    editor.putString("mobile", str_mobile);
                    editor.putBoolean("login", true);
                    editor.putString("mobile", response.body().getPhone());
                    editor.putString("fname", response.body().getfName());
                    editor.putString("lname", response.body().getlName());
                    editor.putString("email", response.body().getEmail());
                    editor.putString("id", response.body().getuId());
                    editor.commit();

                    Intent myint = new Intent(context, HomeActivity.class);
                    myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(myint);
                    ((Activity) context).finish();
                } else {
                    //set the snack message
                    setSnackMessage(response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public void setSnackMessage(String message) {
        txt_error_msg.setText(message);

        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);

        errorLayout.setVisibility(View.VISIBLE);
        errorLayout.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                errorLayout.clearAnimation();

//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                        errorLayout.getWidth(), errorLayout.getHeight());
//                lp.setMargins(0, errorLayout.getWidth(), 0, 0);
//                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                errorLayout.setLayoutParams(lp);
            }
        });
//        expand(errorLayout);
    }

    public void verifyUserId(String otp) {

        Call<APIMobileVerification> userVerify = webServiceController.verify(userId, key, otp);
        userVerify.enqueue(new Callback<APIMobileVerification>() {
            @Override
            public void onResponse(Response<APIMobileVerification> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        progressDialog.setMessage("Verifying user");
                        progressDialog.dismiss();
                        SharedPreferences.Editor editor = mpref.edit();
                        editor.putString("mobile", str_mobile);
                        editor.putBoolean("login", true);
                        editor.commit();

                        Intent myint = new Intent(context, HomeActivity.class);
                        myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(myint);
                        finish();
                    }

                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
