package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bean.PlacesBean;
import com.rovecab.R;

import java.util.List;

/**
 * Created by sujith on 22-03-2016.
 */
public class ListingAdapter extends BaseAdapter {

    List<PlacesBean> beanPlaces = null;
    private Context context = null;

    public ListingAdapter(List<PlacesBean> beanPlaces, Context context) {
        this.beanPlaces = beanPlaces;
        this.context = context;
    }

    @Override
    public int getCount() {
        return beanPlaces.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.adapter_listing_location, null);
        TextView city = (TextView) convertView.findViewById(R.id.city);
        TextView place = (TextView) convertView.findViewById(R.id.place);
        city.setText(beanPlaces.get(position).getCity() + ", " + beanPlaces.get(position).getState());
        place.setText(beanPlaces.get(position).getPlace());
        return convertView;
    }
}
