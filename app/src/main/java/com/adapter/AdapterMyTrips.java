package com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.WebResponse.APITripsList;
import com.rovecab.R;
import com.service.CustomText;

import java.util.List;

/**
 * Created by sujith on 23-05-2016.
 */
public class AdapterMyTrips extends BaseAdapter {

    private Context context = null;
    private List<APITripsList> myTrips = null;

    public AdapterMyTrips(Context context, List<APITripsList> myTrips) {
        this.context = context;
        this.myTrips = myTrips;
    }

    @Override
    public int getCount() {
        return myTrips.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_my_trips, null);
            viewHolder.tripPrice = (TextView) convertView.findViewById(R.id.price);
            viewHolder.tripName = (CustomText) convertView.findViewById(R.id.cab_name);
            viewHolder.tripId = (CustomText) convertView.findViewById(R.id.trip_id);
            viewHolder.tripTime = (CustomText) convertView.findViewById(R.id.trip_date);
            viewHolder.tripStatus = (CustomText) convertView.findViewById(R.id.status);
            viewHolder.cardNo = (CustomText) convertView.findViewById(R.id.cardno);
            viewHolder.cardImage = (ImageView) convertView.findViewById(R.id.card_img);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tripId.setText(myTrips.get(position).getTripId());
        viewHolder.tripName.setText(myTrips.get(position).getCarBrand() +" "+ myTrips.get(position).getCarModel()+" ("+myTrips.get(position).getCarType()+")");
        viewHolder.tripTime.setText(myTrips.get(position).getPickupTime());
        int hex = Integer.parseInt(myTrips.get(position).getCurrencySign(), 16);
        viewHolder.tripPrice.setText((char)hex+""+myTrips.get(position).getTotalFare());

        if(myTrips.get(position).getCardNo()==null || myTrips.get(position).getCardNo().equalsIgnoreCase("")){
            viewHolder.cardImage.setVisibility(View.GONE);
            viewHolder.cardNo.setVisibility(View.GONE);
        }else{
            viewHolder.cardImage.setVisibility(View.VISIBLE);
            viewHolder.cardNo.setVisibility(View.VISIBLE);
            viewHolder.cardNo.setText(myTrips.get(position).getCardNo());
        }
        if(myTrips.get(position).getStatus().equalsIgnoreCase("Order Placed")){
            viewHolder.tripStatus.setTextColor(Color.parseColor("#0095c0"));
            viewHolder.cardImage.setVisibility(View.GONE);
            viewHolder.cardNo.setVisibility(View.GONE);
        }
        else if(myTrips.get(position).getStatus().equalsIgnoreCase("Cancelled")){
            viewHolder.tripStatus.setTextColor(Color.parseColor("#e34000"));
            viewHolder.cardImage.setVisibility(View.GONE);
            viewHolder.cardNo.setVisibility(View.GONE);
        }
        else if(myTrips.get(position).getStatus().equalsIgnoreCase("Completed")){
            viewHolder.tripStatus.setTextColor(Color.parseColor("#009600"));
            if(myTrips.get(position).getCardNo()==null || myTrips.get(position).getCardNo().equalsIgnoreCase("")){
                viewHolder.cardImage.setVisibility(View.GONE);
                viewHolder.cardNo.setVisibility(View.GONE);
            }else{
                viewHolder.cardImage.setVisibility(View.VISIBLE);
                viewHolder.cardNo.setVisibility(View.VISIBLE);
                viewHolder.cardNo.setText(myTrips.get(position).getCardNo());
            }
        }
        else if(myTrips.get(position).getStatus().equalsIgnoreCase("Processing")){
            viewHolder.tripStatus.setTextColor(Color.parseColor("#0095c0"));
            viewHolder.cardImage.setVisibility(View.GONE);
            viewHolder.cardNo.setVisibility(View.GONE);
        }
        else if(myTrips.get(position).getStatus().equalsIgnoreCase("Confirmed")){
            viewHolder.tripStatus.setTextColor(Color.parseColor("#0095c0"));
            viewHolder.cardImage.setVisibility(View.GONE);
            viewHolder.cardNo.setVisibility(View.GONE);
        }
        viewHolder.tripStatus.setText(myTrips.get(position).getStatus());


        return convertView;
    }

    static class ViewHolder{
        CustomText tripId = null;
        CustomText tripName = null;
        CustomText tripTime = null;
        CustomText tripStatus = null;
        CustomText cardNo = null;
        TextView tripPrice = null;
        ImageView cardImage = null;
    }
}
