package com.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fragments.PlaceholderFragment;
import com.fragments.PlaceholderFragment1;
import com.fragments.PlaceholderFragment2;
import com.fragments.PlaceholderFragment3;
import com.fragments.PlaceholderFragment4;

/**
 * Created by sujith on 20-06-2016.
 */
public class PageAdapter extends FragmentStatePagerAdapter {

    private int[] imageUrls = null;

    public PageAdapter(android.support.v4.app.FragmentManager fragmentManager, int url[]) {
        super(fragmentManager);
        this.imageUrls = url;
    }

    @Override
    public Fragment getItem(int position) {
        final Bundle bundle = new Bundle();
        bundle.putInt(PlaceholderFragment.EXTRA_POSITION, position);
        switch (position) {
            case 0:
                PlaceholderFragment fragment = new PlaceholderFragment();
                fragment.setArguments(bundle);
                return fragment;
            case 1:
                PlaceholderFragment1 fragment1 = new PlaceholderFragment1();
                fragment1.setArguments(bundle);
                return fragment1;
            case 2:
                PlaceholderFragment2 fragment2 = new PlaceholderFragment2();
                fragment2.setArguments(bundle);
                return fragment2;
            case 3:
                PlaceholderFragment3 fragment3 = new PlaceholderFragment3();
                fragment3.setArguments(bundle);
                return fragment3;
            case 4:
                PlaceholderFragment4 fragment4 = new PlaceholderFragment4();
                fragment4.setArguments(bundle);
                return fragment4;
//            case 5:
//                PlaceholderFragment fragment5 = new PlaceholderFragment();
//                fragment5.setArguments(bundle);
//                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return imageUrls.length;
    }

}
