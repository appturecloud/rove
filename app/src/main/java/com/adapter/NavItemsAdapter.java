package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rovecab.R;

/**
 * Created by sujith on 13-05-2016.
 */
public class NavItemsAdapter extends BaseAdapter {

    private Context context = null;
    private String[] items = null;
    private int[] navItems = null;

    public NavItemsAdapter(Context context, String[] items, int[] navItems) {
        this.context = context;
        this.items = items;
        this.navItems = navItems;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_nav_drawer, null);
        TextView navItem = (TextView) view.findViewById(R.id.nav_item);
        ImageView navImg = (ImageView) view.findViewById(R.id.nav_img);
        navItem.setText(items[position]);
        navImg.setImageResource(navItems[position]);
        return view;
    }
}
