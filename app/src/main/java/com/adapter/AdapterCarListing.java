package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bean.CabsBean;
import com.rovecab.R;
import com.service.CustomText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 16-05-2016.
 */
public class AdapterCarListing extends BaseAdapter {

    private Context context = null;
    private ArrayList<CabsBean> listCars = null;

    public AdapterCarListing(Context context, ArrayList<CabsBean> listCars) {
        this.context = context;
        this.listCars = listCars;
    }

    @Override
    public int getCount() {
        return listCars.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_car_list, null);
            holder = new ViewHolder();
            holder.modelName = (CustomText) convertView.findViewById(R.id.model_name);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.modelName.setText(listCars.get(position).getName());
        int hex = Integer.parseInt(listCars.get(position).getCurrency(), 16);
        holder.price.setText((char)hex+""+listCars.get(position).getFare());

        return convertView;
    }

    static class ViewHolder{
        CustomText modelName = null;
        TextView price = null;
    }
}
