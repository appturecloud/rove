package com.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.Dialog.DialogDeleteCard;
import com.bean.CardsBean;
import com.interfaces.DismissDialog;
import com.rovecab.Payment_listing;
import com.rovecab.R;
import com.service.AsyncTaskListener;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 8/1/2016.
 */
public class AdapterPaymentMethods extends BaseAdapter implements AsyncTaskListener, DismissDialog {

    private Context context = null;
    private List<CardsBean> cardsBean = null;
    private int selectedIndex = -1;
    private AsyncTaskListener listener = null;
    private List<NameValuePair> nameValuePairs = null;
    private String key = null;
    private String id = null;
    private ProgressDialog progressDialog = null;


    public AdapterPaymentMethods(Context context, List<CardsBean> cardsBean, String uId, String key) {
        this.context = context;
        this.cardsBean = cardsBean;
        listener = AdapterPaymentMethods.this;
        progressDialog = new ProgressDialog(context);
        this.id = uId;
        this.key = key;
    }

    @Override
    public int getCount() {
        return cardsBean.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_payment_listing, null);
            holder.radioCardNo = (RadioButton) convertView.findViewById(R.id.radio_card);
            holder.deleteCard = (LinearLayout) convertView.findViewById(R.id.delete_card);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String uri = null;
        if (cardsBean.get(position).getCardYype().equalsIgnoreCase("american express")) {
            uri = "@drawable/cc_small_americanexpress";
        } else if (cardsBean.get(position).getCardYype().equalsIgnoreCase("maestro")) {
            uri = "@drawable/cc_small_maestro";
        } else if (cardsBean.get(position).getCardYype().equalsIgnoreCase("master card")) {
            uri = "@drawable/cc_small_mastercard";
        } else if (cardsBean.get(position).getCardYype().equalsIgnoreCase("visa")) {
            uri = "@drawable/cc_small_visa";
        } else if (cardsBean.get(position).getCardYype().equalsIgnoreCase("discover")) {
            uri = "@drawable/cc_small_discover";
        } else if (cardsBean.get(position).getCardYype().equalsIgnoreCase("paypal")) {
            uri = "@drawable/cc_small_paypal";
        }
        if (uri != null) {
            int imgRes = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Drawable img = context.getResources().getDrawable(imgRes);
            img.setBounds(0, 0, 2, 2);
            holder.radioCardNo.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        }

        holder.radioCardNo.setText("XXXX - XXXX - XXXX - " + cardsBean.get(position).getCardNo());
        if (cardsBean.get(position).getSelected() == 1) {
            holder.radioCardNo.setChecked(true);
        } else {
            holder.radioCardNo.setChecked(false);
        }

        holder.radioCardNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Payment_listing listing = new Payment_listing();
                    try {
                        listing.setEnabled(cardsBean.get(position).getCardId());
                    } catch (IndexOutOfBoundsException IOE) {

                    }
                }
            }
        });

        holder.deleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedIndex = position;
                DialogDeleteCard delete = new DialogDeleteCard(context, AdapterPaymentMethods.this);
                delete.setCancelable(false);
                delete.show();
            }
        });
        return convertView;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if (jobj != null) {
                if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
                    progressDialog.dismiss();
                    cardsBean.remove(selectedIndex);
                    Payment_listing list = new Payment_listing();
                    list.updateList(cardsBean);
                } else if (jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 103) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Cannot remove a default card", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss(Intent myint) {
        if (myint.getExtras().getBoolean("delete")) {


            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.setMessage("Deleting card");
            progressDialog.show();
            nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("uId", id));
            nameValuePairs.add(new BasicNameValuePair("key", key));
            nameValuePairs.add(new BasicNameValuePair("card", String.valueOf(cardsBean.get(selectedIndex).getCardId())));

            new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/deletesavedcard", nameValuePairs, 2, "deleteCard").execute();
        }
    }

    static class ViewHolder {
        RadioButton radioCardNo = null;
        LinearLayout deleteCard = null;
    }
}
