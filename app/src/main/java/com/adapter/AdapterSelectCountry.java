package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bean.CountryBean;
import com.rovecab.R;

import java.util.List;

/**
 * Created by sujith on 10-06-2016.
 */
public class AdapterSelectCountry extends BaseAdapter {

    private Context context = null;
    private List<CountryBean> countryBean = null;

    public AdapterSelectCountry(Context context, List<CountryBean> countryBean) {
        this.context = context;
        this.countryBean = countryBean;
    }

    @Override
    public int getCount() {
        return countryBean.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_select_country, null);
        TextView country = (TextView) view.findViewById(R.id.txt_country);
        ImageView imgCountry = (ImageView) view.findViewById(R.id.img_country);

        country.setText(countryBean.get(position).getName());
        int res = context.getResources().getIdentifier(countryBean.get(position).getFlag(), "drawable", context.getPackageName());
        imgCountry.setImageResource(res);
        return view;
    }
}
