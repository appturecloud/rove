package com.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bean.CabsBean;
import com.fragments.SliderAdapter;

import java.util.ArrayList;

/**
 * Created by sujith on 09-05-2016.
 */
public class SlideUpPagerAdapter extends FragmentStatePagerAdapter {

    private String url = null;
    private ArrayList<CabsBean> bean = null;
    private int position = 0;

    public SlideUpPagerAdapter(FragmentManager fm, ArrayList<CabsBean> bean1, int position, String url) {
        super(fm);
        this.url = url;
        this.bean = bean1;
        this.position = position;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new SliderAdapter();
        Bundle bundle = new Bundle();
        bundle.putString("id", url);
        bundle.putSerializable("cabs", bean);
        bundle.putInt("position", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

}
