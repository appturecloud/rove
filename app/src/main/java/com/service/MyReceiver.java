package com.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.rovecab.ActivityMobileVerification;


public class MyReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        Bundle bundle = intent.getExtras();
        ActivityMobileVerification verif2 = new ActivityMobileVerification();
        final Object[] pdusObj = (Object[]) bundle.get("pdus");

        for (int i = 0; i < pdusObj.length; i++) {

            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            String phoneNumber = currentMessage.getDisplayOriginatingAddress();

            String senderNum = phoneNumber;

            if (senderNum.startsWith("DM-02")) {
                String message = currentMessage.getDisplayMessageBody();
                try {
                    verif2.validateOTP(context, message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
