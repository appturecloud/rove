package com.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.Protocol;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;

/**
 * HTTPAsync
 * 
 * Responsible for posting commands data to server
 * @author Nipun
 * @version $Revision: 1.0 $
 */
public class HttpAsync extends AsyncTask<Void, Void, String>{
	
	public static final String TAG = "HTTPAsync";
	public String Tag = "";

	private AsyncTaskListener mCallback = null;
	private String mUrl = "";
	private int callType = 0;
	private List<NameValuePair> nameValuePair;

	/**
	 * Constructor for HttpAsync.
	 * @param context Context
	 * @param callback AsyncTaskListener
	 * @param baseURL String
	 * @param nameValuePair List<NameValuePair>
	 * @param calltype int
	 * @param Tag String
	 */
	public HttpAsync(Context context, AsyncTaskListener callback,
			String baseURL, List<NameValuePair> nameValuePair, int calltype,String Tag) {
		mCallback = callback;
		mUrl = baseURL;
		this.nameValuePair = nameValuePair;
		this.callType = calltype;
		this.Tag = Tag;
	}

/*	public HttpAsync(Context context, AsyncTaskListener callback,
			String baseURL, String search_text, int serviceType,int calltype,String Tag) {
		// TODO Auto-generated constructor stub
		mCallback = callback;
		mUrl = baseURL;
		this.sSearch = search_text;
		this.serviceType = serviceType;
		this.callType = calltype;
		this.Tag = Tag;
	}*/

	/**
 * Method doInBackground.
 * @param params Void[]
 * @return String
 */
@Override
	protected String doInBackground(Void... params) {
		// TODO We have to form the parameters as payload
		String result = null;
		switch (callType) {
		case 1:
			result = httpRestPostCall();
			break;
		case 2:
			result = httpRestGetCall();
			break;
		}
		return result;
	}

	

	/**
	 * Method onPostExecute.
	 * @param result String
	 */
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		//Log.d(TAG, result);
		if(mCallback != null){
			if (result != null ) {
				mCallback.onTaskComplete(result, Tag);
			} else {
				mCallback.onTaskCancelled(null);
			}
		}
		
		
	}
	
	/**
	 * Method httpRestGetCall.
	 * @return String
	 */
	public String httpRestGetCall() {

		String result = "";
		try {
			// Creating HTTP client

			DefaultHttpClient httpClient = new DefaultHttpClient();
			String paramsString = URLEncodedUtils
					.format(nameValuePair, "UTF-8");

			Log.e("mUrl", mUrl+"?"+paramsString);
			// Creating HTTP Post
			HttpGet httpGet = new HttpGet(mUrl + "?" + paramsString);

//			httpGet.setHeader("Origin", "http://local.dev");
			// Url Encoding the GET parameters
			// Making HTTP Request
			HttpResponse response = httpClient.execute(httpGet);
			// writing response to log
			result = EntityUtils.toString(response.getEntity());

		} catch (ClientProtocolException e) {
			// writing exception to log
			result = "fail";
			e.printStackTrace();
		} catch (IOException e) {
			// writing exception to log
			result = "fail";
			e.printStackTrace();
		}
		return result;
	}


	/**
	 * Method httpRestPostCall.
	 * @return String
	 */
	public String httpRestPostCall() {
		String result = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(mUrl);

		try {
			// Add your data
			// Execute HTTP Post Request
			// HttpResponse response = httpclient.execute(httppost);

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));

			// Execute HTTP Post Request
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpclient.execute(httppost, responseHandler);
			result = responseBody.replaceAll("﻿", "");

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e("protocol exception", "");
		}

		catch (UnsupportedEncodingException exception) {
			result = "fail";
			exception.printStackTrace();
		} catch (IllegalArgumentException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (HttpHostConnectException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (ConnectTimeoutException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (SocketTimeoutException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (SocketException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (IOException exception) {
			exception.printStackTrace();
			result = "fail";
		} catch (Exception exception) {
			exception.printStackTrace();
			result = "fail";
		} finally {

		}

		return result;
	}


}
