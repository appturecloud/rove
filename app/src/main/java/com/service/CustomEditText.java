package com.service;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.rovecab.R;

public class CustomEditText extends EditText {

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}



	public void init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.customfont);
		String fontFamily = null;
		int fontStyle = 0;
		final int n = a.getIndexCount();

		for (int i = 0; i < n; ++i) {
			int attr = a.getIndex(i);
			if (attr == R.styleable.customfont_android_fontFamily) {
				fontFamily = a.getString(attr);
			}
//			if(attr == R.styleable.customfont_android_textStyle){
//				fontStyle = a.getString(attr);
//				Log.e("font style is", fontStyle);
//			}
			a.recycle();
		}
		if(n<1){
			fontFamily = "TitilliumWeb-Regular.ttf";
		}
		if (!isInEditMode()) {
			try {
				Typeface tf = Typeface.createFromAsset(getContext().getAssets(), fontFamily);

				setTypeface(tf);
//				if(fontStyle.co){
//					setTypeface(tf, Typeface.BOLD);
//				}
//				else{
//					setTypeface(tf);
//				}
			} catch (Exception e) {
			}
		}
	}
}