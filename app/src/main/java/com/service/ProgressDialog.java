package com.service;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ProgressBar;

import com.rovecab.R;


/**
 * Created by sujith on 14-04-2016.
 */
public class ProgressDialog extends Dialog {

    private Context context = null;
    private ProgressBar progressBar = null;
    public ProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.progress_dialog);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        Drawable draw=context.getResources().getDrawable(R.drawable.custom_progressbar);
        progressBar.setProgressDrawable(draw);

    }
}
