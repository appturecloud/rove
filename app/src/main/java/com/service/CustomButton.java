package com.service;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.rovecab.R;

/**
 * Created by sujith on 19-05-2016.
 */
public class CustomButton extends Button {

    public CustomButton(Context context) {
        super(context);
        setCustomFont(this, context, null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(this, context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(this, context, attrs);
    }

    public void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.customfont);
        String font = a.getString(R.styleable.customfont_android_fontFamily);
        if (font == null) {
            font = "TitilliumWeb-Regular.ttf";
        }
        try {
            Typeface tf = Typeface.createFromAsset(
                    getContext().getAssets(), font);
            setTypeface(tf);
        } catch (Exception e) {
        }
        a.recycle();
    }

}
