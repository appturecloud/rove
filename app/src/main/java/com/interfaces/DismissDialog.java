package com.interfaces;

import android.content.Intent;

/**
 * Created by sujith on 10-06-2016.
 */
public interface DismissDialog {
    public void dismiss(Intent myint);
}
