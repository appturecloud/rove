package com.interfaces;


import com.WebResponse.APIChangePassword;
import com.WebResponse.APIForgotPassword;
import com.WebResponse.APILogin;
import com.WebResponse.APIMobileVerification;
import com.WebResponse.APIMyTrips;
import com.WebResponse.APIOTPResend;
import com.WebResponse.APIRegister;
import com.WebResponse.APIResendOTP;
import com.WebResponse.APIResetPassword;
import com.WebResponse.APISaveProfile;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by sujith on 12-05-2016.
 */
public interface WebServiceController {

    @GET("service/signup?")
    Call<APIRegister> register(@Query("country") String country, @Query("first_name") String first_name, @Query("last_name") String last_name,
                               @Query("email") String email,@Query("password") String password, @Query("phone") String phone, @Query("source") String source, @Query("ref_code") String ref_code);

    @GET("service/signin?")
    Call<APILogin> login(@Query("country") String country, @Query("password") String password, @Query("phone") String phone, @Query("email") String email);

    @GET("service/verifyuser?")
    Call<APIMobileVerification> verify(@Query("uId") String userId, @Query("key") String key, @Query("otp") String otp);

    @GET("service/generateotp?")
    Call<APIResendOTP> generateOTP(@Query("uId") String userId, @Query("key") String key);  // in login page mobile verification

    @GET("service/recoverpassword?")
    Call<APIForgotPassword> forgotPassword(@Query("country") String country, @Query("phone") String phone);

    @GET("service/resendotp?")
    Call<APIOTPResend> resendOTP(@Query("country") String country, @Query("phone") String phone);

    @GET("rove/service/setpassword?")
    Call<APIResetPassword> resetPassword(@Query("password") String password, @Query("resetToken") String resetToken, @Query("otp") String otp);

    @GET("service/saveprofile?")
    Call<APISaveProfile> saveProfile(@Query("uId") String user_id, @Query("key") String key, @Query("first_name") String fname, @Query("last_name") String lname,
                                     @Query("email") String email, @Query("phone") String phone);

    @GET("service/changepassword?")
    Call<APIChangePassword> changePassword(@Query("uId") String userId, @Query("key") String key, @Query("cPassword") String cPassword, @Query("nPassword") String nPassword);

//    @GET("rove/search-rides?")
//    @GET("rove/site/loadmytrips?")
//    Call<APIMyTrips> myTrips (@Query("email") String email);

}
