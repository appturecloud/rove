package com.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.interfaces.DismissDialog;
import com.rovecab.R;
import com.service.CustomText;

/**
 * Created by sujith on 8/11/2016.
 */
public class DialogAddCard extends Dialog {

    private Context context = null;
    private ListView listValues = null;
    private String[] values = null;
    private String text = null;
    private ArrayAdapter<String> adapter = null;
    private DismissDialog dialogDismiss = null;
    private boolean isMonth = false;
    private CustomText header = null;

    public DialogAddCard(Context context, DismissDialog dialog, String[] val, String id, boolean isMonth) {
        super(context);
        this.context = context;
        this.dialogDismiss = dialog;
        this.values = val;
        this.text = id;
        this.isMonth = isMonth;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_card);

        header = (CustomText) findViewById(R.id.headers);
        listValues = (ListView) findViewById(R.id.list_values);

        header.setText(text);
        adapter = new ArrayAdapter<String>(context, R.layout.adapter_payment_year_month, values);
        listValues.setAdapter(adapter);

        listValues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myint = new Intent();
                myint.putExtra("val", values[position]);
                myint.putExtra("id", id);
                myint.putExtra("month", isMonth);
                dialogDismiss.dismiss(myint);
                dismiss();
            }
        });
    }
}

