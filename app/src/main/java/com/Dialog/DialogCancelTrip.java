package com.Dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.interfaces.DismissDialog;
import com.rovecab.R;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sujith on 7/19/2016.
 */
public class DialogCancelTrip extends Dialog implements AsyncTaskListener {

    private String price = null;
    private String currecySign = null;
    private String currencyCode = null;
    private String tripId = null;
    private String key = null;
    private String uId = null;

    private CustomButton dismiss = null;
    private CustomButton confirmCancel = null;
    private CustomText cancelationFee = null;


    private Context context = null;
    private AsyncTaskListener listener = null;
    private DismissDialog dismissDialog = null;
    private ProgressDialog progressDialog = null;
    private ArrayList<NameValuePair> nameValuePairs = null;


    public DialogCancelTrip(Context context, String price, String currencyCode, String code, String tripId, String uId, String key, DismissDialog dialog) {
        super(context);
        this.context = context;
        this.price = price;
        this.currecySign = currencyCode;
        this.currencyCode = code;
        this.tripId = tripId;
        this.uId = uId;
        this.key = key;
        dismissDialog = dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_cancel_trip);
        int hex = Integer.parseInt(currecySign, 16);
        cancelationFee = (CustomText) findViewById(R.id.cancelation_fee);
        dismiss = (CustomButton) findViewById(R.id.no);
        confirmCancel = (CustomButton) findViewById(R.id.yes);

        cancelationFee.setText((char) hex + "" + price + " (" + currencyCode + ")");

        listener = DialogCancelTrip.this;
        progressDialog = new ProgressDialog(context);
        confirmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                progressDialog.setMessage("Canceling trip");
                progressDialog.setIndeterminate(false);
                nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("trip", tripId));
                nameValuePairs.add(new BasicNameValuePair("key", key));
                nameValuePairs.add(new BasicNameValuePair("uId", uId));
                new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/confirmtripcancellation", nameValuePairs, 2, "confirm-cancel-trip").execute();
            }
        });

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        Log.e("result", result);
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject != null) {
                if (jsonObject.getString("status").equalsIgnoreCase("success") && jsonObject.getInt("code") == 1) {
                    progressDialog.dismiss();
                    Intent myint = new Intent();
                    myint.putExtra("isCancelled", true);
                    dismissDialog.dismiss(myint);
                    dismiss();
                }
                else{
                    progressDialog.dismiss();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
