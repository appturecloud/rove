package com.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.adapter.AdapterSelectCountry;
import com.bean.CountryBean;
import com.interfaces.DismissDialog;
import com.rovecab.R;

import java.util.List;

/**
 * Created by sujith on 18-04-2016.
 */
public class SelectCountryDialog extends Dialog {

    private Context context = null;
    private List<CountryBean> bean = null;
    private ListView listView = null;
    private Intent myint = null;
    private DismissDialog dismissDialog = null;

    public SelectCountryDialog(Context context, List<CountryBean> bean, DismissDialog dialog) {
        super(context);
        this.context = context;
        this.bean = bean;
        this.dismissDialog = dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_select_country);
        listView = (ListView) findViewById(R.id.select_country);

        AdapterSelectCountry country = new AdapterSelectCountry(context, bean);
        listView.setAdapter(country);
        myint = new Intent();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                myint.putExtra(context.getString(R.string.country), "+" + bean.get(position).getCountry_code());
                myint.putExtra(context.getString(R.string.flag), bean.get(position).getFlag());
                myint.putExtra("country_id", bean.get(position).getCountry_id());
                dismissDialog.dismiss(myint);
                dismiss();
            }
        });
    }


}
