package com.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.interfaces.DismissDialog;
import com.rovecab.R;
import com.service.CustomButton;

/**
 * Created by sujith on 7/19/2016.
 */
public class DialogDeleteCard extends Dialog {

    private CustomButton dismiss = null;
    private CustomButton confirmCancel = null;

    private Context context = null;
    private DismissDialog dismissDialog = null;

    public DialogDeleteCard(Context context, DismissDialog dialog) {
        super(context);
        this.context = context;
        dismissDialog = dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_delete_card);
        dismiss = (CustomButton) findViewById(R.id.no);
        confirmCancel = (CustomButton) findViewById(R.id.yes);

        confirmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent();
                myint.putExtra("delete", true);
                dismissDialog.dismiss(myint);
                dismiss();
            }
        });

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
