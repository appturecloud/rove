package com.Dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.rovecab.HomeActivity;
import com.rovecab.R;
import com.rovecab.add_payment;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomEditText;
import com.service.HttpAsync;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by sujith on 8/9/2016.
 */
public class DialogRequestConfirmation extends Dialog implements AsyncTaskListener {

    private Context context = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;
    private List<NameValuePair> nameValuePair = null;

    private CustomEditText edtNotes = null;
    private CustomButton btnRequest = null;
    private CustomButton btnDismiss = null;

    public DialogRequestConfirmation(Context context, AsyncTaskListener listener, List<NameValuePair> nameValuePairs) {
        super(context);
        this.context = context;
        this.listener = listener;
        nameValuePair = nameValuePairs;
        this.listener = DialogRequestConfirmation.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pre_confirmation);

        edtNotes = (CustomEditText) findViewById(R.id.edt_notes);
        btnDismiss = (CustomButton) findViewById(R.id.dismiss);
        btnRequest = (CustomButton) findViewById(R.id.request);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Requesting ride");
        progressDialog.setIndeterminate(false);

        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameValuePair.add(new BasicNameValuePair("notes", edtNotes.getText().toString()));
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                progressDialog.show();
                callApi(String.valueOf(0));
            }
        });

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                dismiss();
            }
        });
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {

        try {
            JSONObject jobj = new JSONObject(result);
            if(jobj!=null){
                    progressDialog.dismiss();
                    dismiss();
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 11) {
                        HomeActivity.fragmentManager.beginTransaction().remove(HomeActivity.fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
                        HomeActivity.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        HomeActivity.fragmentManager.popBackStack();
                        dismiss();
                        DialogConfirmation confirmation = new DialogConfirmation(context);
                        confirmation.show();
                    }
                    else if(jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 100){
                        progressDialog.dismiss();
                        //no payment added
                        Intent myint = new Intent(context, add_payment.class);
                        myint.putExtra("request", true);
                        context.startActivity(myint);
                    }
                    else if(jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 10){
                        callApi(tag);
//                        progressDialog.show();
//                        new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/requestride", nameValuePair, 2, "request-ride").execute();
                    }
                    else{
                        Toast.makeText(context, jobj.getString("msg"), Toast.LENGTH_LONG).show();
                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void callApi(String attempt){
        Log.e("attempt", attempt);
        if(Integer.parseInt(attempt)<=3){
            new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/requestride", nameValuePair, 2, String.valueOf(Integer.parseInt(attempt)+1)).execute();
        }
        else{
            progressDialog.dismiss();
            Toast.makeText(context, "Unable to Process your request", Toast.LENGTH_SHORT).show();
        }
    }
}
