package com.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.rovecab.ActivityMyTrip;
import com.rovecab.R;
import com.service.CustomButton;

/**
 * Created by sujith on 22-05-2016.
 */
public class DialogConfirmation extends Dialog {

    private CustomButton btnCancel = null;
    private CustomButton btnMyTrips = null;
    private CustomButton btnCall = null;
    private Context context = null;

    public DialogConfirmation(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirmation);

//        btnCall = (CustomButton) findViewById(R.id.call_driver);
        btnCancel = (CustomButton) findViewById(R.id.dismiss);
        btnMyTrips = (CustomButton) findViewById(R.id.mytrips);

//        btnCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        btnMyTrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myint = new Intent(context, ActivityMyTrip.class);
                myint.setFlags(myint.FLAG_ACTIVITY_CLEAR_TOP);
                myint.putExtra("home", false);
                context.startActivity(myint);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
