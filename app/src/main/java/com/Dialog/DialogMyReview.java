package com.Dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.rovecab.R;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomText;
import com.service.HttpAsync;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 23-05-2016.
 */
public class DialogMyReview extends Dialog implements AsyncTaskListener {

    private Context context = null;
    private List<NameValuePair> nameValuePairs = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;

    private boolean isReview = false;
    private int ratings = 0;
    private int userRatings = 0;
    private String review = null;
    private String tripId = null;
    private String key = null;
    private String uId = null;

    private ImageView ReviewStar1 = null;
    private ImageView ReviewStar2 = null;
    private ImageView ReviewStar3 = null;
    private ImageView ReviewStar4 = null;
    private ImageView ReviewStar5 = null;
    private CustomText edtReview = null;
    private CustomButton submit = null;


    public DialogMyReview(Context context, boolean isReview, String review, int ratings, String tripId, String uId, String key) {
        super(context);
        this.context = context;
        this.isReview = isReview;
        this.review = review;
        this.ratings = ratings;
        this.tripId = tripId;
        this.key = key;
        this.uId = uId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_my_review);

        listener = DialogMyReview.this;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Submitting review");
        progressDialog.setIndeterminate(false);

        ReviewStar1 = (ImageView) findViewById(R.id.star1);
        ReviewStar2 = (ImageView) findViewById(R.id.star2);
        ReviewStar3 = (ImageView) findViewById(R.id.star3);
        ReviewStar4 = (ImageView) findViewById(R.id.star4);
        ReviewStar5 = (ImageView) findViewById(R.id.star5);
        submit = (CustomButton) findViewById(R.id.submit);
        edtReview = (CustomText) findViewById(R.id.edt_review);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (submit.getText().toString().equalsIgnoreCase("submit")) {
                    nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("key", key));
                    nameValuePairs.add(new BasicNameValuePair("id", uId));
                    nameValuePairs.add(new BasicNameValuePair("trip", tripId));
                    nameValuePairs.add(new BasicNameValuePair("review", edtReview.getText().toString()));
                    nameValuePairs.add(new BasicNameValuePair("rating", String.valueOf(userRatings)));
                    progressDialog.show();
                    new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "site/postreview", nameValuePairs, 2, null).execute();
                }
            }
        });

        if (isReview) {
            submit.setText("DISMISS");
            edtReview.setText(review);
            edtReview.setEnabled(false);
            if (ratings == 0) {
                ReviewStar1.setImageResource(R.drawable.star_normal);
                ReviewStar2.setImageResource(R.drawable.star_normal);
                ReviewStar3.setImageResource(R.drawable.star_normal);
                ReviewStar4.setImageResource(R.drawable.star_normal);
                ReviewStar5.setImageResource(R.drawable.star_normal);
            }
            if (ratings == 1) {
                ReviewStar1.setImageResource(R.drawable.star_selected);
                ReviewStar2.setImageResource(R.drawable.star_normal);
                ReviewStar3.setImageResource(R.drawable.star_normal);
                ReviewStar4.setImageResource(R.drawable.star_normal);
                ReviewStar5.setImageResource(R.drawable.star_normal);
            }
            if (ratings == 2) {
                ReviewStar1.setImageResource(R.drawable.star_selected);
                ReviewStar2.setImageResource(R.drawable.star_selected);
                ReviewStar3.setImageResource(R.drawable.star_normal);
                ReviewStar4.setImageResource(R.drawable.star_normal);
                ReviewStar5.setImageResource(R.drawable.star_normal);
            }
            if (ratings == 3) {
                ReviewStar1.setImageResource(R.drawable.star_selected);
                ReviewStar2.setImageResource(R.drawable.star_selected);
                ReviewStar3.setImageResource(R.drawable.star_selected);
                ReviewStar4.setImageResource(R.drawable.star_normal);
                ReviewStar5.setImageResource(R.drawable.star_normal);
            }
            if (ratings == 4) {
                ReviewStar1.setImageResource(R.drawable.star_selected);
                ReviewStar2.setImageResource(R.drawable.star_selected);
                ReviewStar3.setImageResource(R.drawable.star_selected);
                ReviewStar4.setImageResource(R.drawable.star_selected);
                ReviewStar5.setImageResource(R.drawable.star_normal);
            }
            if (ratings == 5) {
                ReviewStar1.setImageResource(R.drawable.star_selected);
                ReviewStar2.setImageResource(R.drawable.star_selected);
                ReviewStar3.setImageResource(R.drawable.star_selected);
                ReviewStar4.setImageResource(R.drawable.star_selected);
                ReviewStar5.setImageResource(R.drawable.star_selected);
            }
            ReviewStar1.setEnabled(false);
            ReviewStar2.setEnabled(false);
            ReviewStar3.setEnabled(false);
            ReviewStar4.setEnabled(false);
            ReviewStar5.setEnabled(false);
        }

        ReviewStar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isReview) {
                    userRatings = 1;
                    ReviewStar1.setImageResource(R.drawable.star_selected);
                    ReviewStar2.setImageResource(R.drawable.star_normal);
                    ReviewStar3.setImageResource(R.drawable.star_normal);
                    ReviewStar4.setImageResource(R.drawable.star_normal);
                    ReviewStar5.setImageResource(R.drawable.star_normal);
                }

            }
        });

        ReviewStar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isReview) {
                    userRatings = 2;
                    ReviewStar1.setImageResource(R.drawable.star_selected);
                    ReviewStar2.setImageResource(R.drawable.star_selected);
                    ReviewStar3.setImageResource(R.drawable.star_normal);
                    ReviewStar4.setImageResource(R.drawable.star_normal);
                    ReviewStar5.setImageResource(R.drawable.star_normal);
                }

            }
        });

        ReviewStar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isReview) {
                    userRatings = 3;
                    ReviewStar1.setImageResource(R.drawable.star_selected);
                    ReviewStar2.setImageResource(R.drawable.star_selected);
                    ReviewStar3.setImageResource(R.drawable.star_selected);
                    ReviewStar4.setImageResource(R.drawable.star_normal);
                    ReviewStar5.setImageResource(R.drawable.star_normal);
                }
            }
        });

        ReviewStar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isReview) {
                    userRatings = 4;
                    ReviewStar1.setImageResource(R.drawable.star_selected);
                    ReviewStar2.setImageResource(R.drawable.star_selected);
                    ReviewStar3.setImageResource(R.drawable.star_selected);
                    ReviewStar4.setImageResource(R.drawable.star_selected);
                    ReviewStar5.setImageResource(R.drawable.star_normal);
                }
            }
        });

        ReviewStar5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isReview) {
                    userRatings = 5;
                    ReviewStar1.setImageResource(R.drawable.star_selected);
                    ReviewStar2.setImageResource(R.drawable.star_selected);
                    ReviewStar3.setImageResource(R.drawable.star_selected);
                    ReviewStar4.setImageResource(R.drawable.star_selected);
                    ReviewStar5.setImageResource(R.drawable.star_selected);
                }
            }
        });

    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {

        try {
            JSONObject jobj = new JSONObject(result);
            if (jobj != null) {
                if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
                    progressDialog.dismiss();
                    dismiss();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
