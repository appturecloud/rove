package com.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.bean.CountryBean;
import com.rovecab.ActivityLogin;
import com.rovecab.R;
import com.rovecab.SplashActivity;
import com.service.CustomButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sujith on 8/16/2016.
 */
public class DialogLogout extends Dialog {

    private Context context = null;

    private CustomButton btnLogout = null;
    private CustomButton btnCancel = null;

    private ArrayList<CountryBean> bean = null;
    private SharedPreferences mpref = null;

    public DialogLogout(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_layout_logout);
        btnLogout = (CustomButton) findViewById(R.id.logout);
        btnCancel = (CustomButton) findViewById(R.id.cancel);

        mpref = context.getSharedPreferences("user_details", Context.MODE_PRIVATE);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean = new ArrayList<CountryBean>();
                StringBuilder builder = new StringBuilder();
                try {
                    InputStream inputStream = context.getAssets().open("phone.txt");
                    BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    String str;
                    while ((str = buffer.readLine()) != null) {
                        builder.append(str);
                    }

                    inputStream.close();

                    JSONObject jobj = new JSONObject(builder.toString());
                    if (jobj != null) {
                        JSONArray jarr = jobj.getJSONArray("countries");
                        if (jarr.length() > 0) {
                            for (int i = 0; i < jarr.length(); i++) {
                                JSONObject jsonObject = jarr.getJSONObject(i);
                                CountryBean bean1 = new CountryBean();
                                bean1.setName(jsonObject.getString("country_name"));
                                bean1.setCountry_id(jsonObject.getString("id"));
                                bean1.setCountry_code(jsonObject.getString("country_code"));
                                bean1.setFlag(jsonObject.getString("country_flag"));
                                bean.add(bean1);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                SharedPreferences.Editor edt = mpref.edit();
                edt.putBoolean("login", false);
                edt.commit();

                Intent myint = new Intent(context, SplashActivity.class);
                myint.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                myint.putExtra("countries", (Serializable) bean);
                context.startActivity(myint);
                ((Activity)context).finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
