package com.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.rovecab.R;
import com.service.CustomButton;
import com.service.CustomText;


/**
 * Created by sujith on 8/10/2016.
 */
public class DialogNotes extends Dialog {

    private Context context = null;
    private String notes = null;

    private CustomText txtNotes = null;
    private CustomButton btnOk = null;

    public DialogNotes(Context context, String notes) {
        super(context);
        this.notes = notes;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_ride_note);
        txtNotes = (CustomText) findViewById(R.id.txt_notes);
        btnOk = (CustomButton) findViewById(R.id.ok);

        txtNotes.setText(notes);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
