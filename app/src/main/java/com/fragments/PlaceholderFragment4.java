package com.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.rovecab.R;

/**
 * Created by sujith on 20-06-2016.
 */
public class PlaceholderFragment4 extends Fragment {

    public static final String EXTRA_POSITION = "EXTRA_POSITION";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int position = getArguments().getInt(EXTRA_POSITION);
        LinearLayout localLinearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_intro_page5, container, false);

        ImageView image = (ImageView) localLinearLayout.findViewById(R.id.image);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.splash_scr5);
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*0.4), (int)(bitmap.getHeight()*0.4), true);
        image.setImageBitmap(scaled);
//            image.setImageResource(imageUrls[position]);

        return localLinearLayout;
    }
}
