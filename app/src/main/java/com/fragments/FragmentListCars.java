package com.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.adapter.AdapterCarListing;
import com.bean.CabsBean;
import com.rovecab.HomeActivity;
import com.rovecab.R;
import com.service.CustomText;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujith on 8/2/2016.
 */
@SuppressLint("ValidFragment")
public class FragmentListCars extends android.support.v4.app.Fragment {
    private ListView listCars = null;
    private CustomText txtCurrType = null;
    private CustomText noCabInfo = null;
    private RelativeLayout layoutHeaders = null;
    private ArrayList<CabsBean> cabsArray = null;

    private LinearLayout.LayoutParams layoutParam = null;
    private AdapterCarListing adapterCarListing = null;

    private int height = 0;
    private String result = null;
    private String currency = null;
    private String userId = null;
    private String key = null;
    private Bundle bundle = null;

    private Context context = null;
    private SharedPreferences mpref = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_list, container, false);
        listCars = (ListView) view.findViewById(R.id.list_cars);
        txtCurrType = (CustomText) view.findViewById(R.id.curr_type);
        noCabInfo = (CustomText) view.findViewById(R.id.no_cab_info);
        layoutHeaders = (RelativeLayout) view.findViewById(R.id.headers);
        bundle = this.getArguments();

        context = getActivity();

        mpref = context.getSharedPreferences("user_details", Context.MODE_PRIVATE);
        userId = mpref.getString("id", null);
        key = mpref.getString("key", null);
        if (bundle != null) {
            result = bundle.getString("result");
        }

        height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, getResources().getDisplayMetrics());
        layoutParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);

        if (result != null) {
            try {
                JSONObject jobj = new JSONObject(result);
                if (jobj != null) {
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {

                        JSONObject jsonObject = jobj.getJSONObject("tripDetails");
                        if (jsonObject != null) {
                            currency = jsonObject.getString("currencySign");
                            txtCurrType.setText("Fare (" + jsonObject.getString("currencyCode") + ")");
                        }
                        JSONArray jarr = jobj.optJSONArray("rides");
                        if (jarr != null && jarr.length() > 0) {
                            listCars.setVisibility(View.VISIBLE);
                            noCabInfo.setVisibility(View.GONE);
                            if (jarr.length() > 8 && jarr.length() > 0) {
                                listCars.setLayoutParams(layoutParam);
                                listCars.requestLayout();
                            }
                            cabsArray = new ArrayList<>();
                            for (int i = 0; i < jarr.length(); i++) {
                                JSONObject jo = jarr.getJSONObject(i);
                                CabsBean bean = new CabsBean();
                                bean.setName(jo.getString("modelName"));
                                bean.setCurrency(currency);
                                bean.setImageName(jo.optString("image"));
                                bean.setId(jo.getInt("id"));
                                bean.setFare(jo.getInt("fare"));
                                bean.setTip(jo.optInt("tip"));
                                cabsArray.add(bean);
//                                }
                            }
                            noCabInfo.setVisibility(View.GONE);
                            listCars.setVisibility(View.VISIBLE);
                            layoutHeaders.setVisibility(View.VISIBLE);
                            adapterCarListing = new AdapterCarListing(getActivity(), cabsArray);
                            listCars.setAdapter(adapterCarListing);
                        } else {
                            noCabInfo.setVisibility(View.VISIBLE);
                            listCars.setVisibility(View.GONE);
                            layoutHeaders.setVisibility(View.GONE);
                        }
                    } else if (jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 110) {
                        noCabInfo.setVisibility(View.VISIBLE);
                        noCabInfo.setText(jobj.getString("msg"));
                        listCars.setVisibility(View.GONE);
                        layoutHeaders.setVisibility(View.GONE);
                    }

                    HomeActivity act = new HomeActivity();
                    act.expandSlidingUp();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        listCars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.support.v4.app.Fragment fragment = new CarSpecificationFragment();
                Bundle bundle = new Bundle();
                bundle.putString("result", result);
                bundle.putSerializable("cabs", cabsArray);
                bundle.putString("id", userId);
                bundle.putString("key", key);
                bundle.putInt("position", position);
                fragment.setArguments(bundle);
                HomeActivity.fragmentManager.beginTransaction().replace(R.id.fragments_cars, fragment).addToBackStack("list").commit();
            }
        });

        return view;
    }
}
