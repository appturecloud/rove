package com.fragments;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.rovecab.HomeActivity;

/**
 * Created by sujith on 11-05-2016.
 */
public class CustomMapView extends MapView {

    private int DELAY = 250;
    private int fingers = 0;
    private GoogleMap googleMap;
    private long lastZoomTime = 0;
    private float lastSpan = -1;
    private Handler handler = new Handler();
    private Handler handleView = new Handler();
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;
    private Runnable runnable = null;
    private long lastTouchTime = -1;

    public CustomMapView(Context context) {
        super(context);
    }

    public CustomMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomMapView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
    }

    public CustomMapView(Context context, GoogleMapOptions options) {
        super(context, options);
    }

    public void init(final GoogleMap map) {


        runnable = new Runnable() {
            @Override
            public void run() {
                HomeActivity homeAct = new HomeActivity();
                homeAct.hideItems();
            }
        };

        scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                if (lastSpan == -1) {
                    lastSpan = detector.getCurrentSpan();
                } else if (detector.getEventTime() - lastZoomTime >= 50) {
                    lastZoomTime = detector.getEventTime();
                    googleMap.animateCamera(CameraUpdateFactory.zoomBy(getZoomValue(detector.getCurrentSpan(), lastSpan)), 50, null);
                    lastSpan = detector.getCurrentSpan();
                }
                return false;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                lastSpan = -1;
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                lastSpan = -1;

            }
        });
        gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {

                switch (e.getAction()) {

//                    case MotionEvent.
//                      break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;

                    case MotionEvent.ACTION_POINTER_UP:
                        break;

                    case MotionEvent.ACTION_UP:
                        break;


                    case MotionEvent.ACTION_DOWN:
                        //double tap to zoom in
                        googleMap.animateCamera(CameraUpdateFactory.zoomIn(), 400, null);
                        break;
                }
                HomeActivity homeAct = new HomeActivity();
//                homeAct.hideItems();
                handleView.postDelayed(runnable, DELAY);
                disableScrolling();

                return true;
            }
        });
        googleMap = map;
    }

    private float getZoomValue(float currentSpan, float lastSpan) {
        double value = (Math.log(currentSpan / lastSpan) / Math.log(1.55d));
        return (float) value;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (ev.getAction() == MotionEvent.ACTION_DOWN) {

            long thisTime = System.currentTimeMillis();
            if (thisTime - lastTouchTime < 250) {

                handleView.postDelayed(runnable, DELAY);
                googleMap.animateCamera(CameraUpdateFactory.zoomIn(), 400, null);
                // Double tap
//                this.getController().zoomInFixing((int) ev.getX(), (int) ev.getY());
                lastTouchTime = -1;

            } else {

                // Too slow
                lastTouchTime = thisTime;
            }
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public void setOnScrollChangeListener(OnScrollChangeListener l) {
        super.setOnScrollChangeListener(l);
    }

    @Override
    public boolean dispatchDragEvent(DragEvent event) {
        return super.dispatchDragEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        gestureDetector.onTouchEvent(ev);
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_POINTER_DOWN:  //5
                fingers = fingers + 1;
                break;
            case MotionEvent.ACTION_POINTER_UP: //6
                fingers = fingers - 1;
                break;
            case MotionEvent.ACTION_UP:     //1
                handleView.removeCallbacks(runnable);
                new HomeActivity().showItems();
                HomeActivity act = new HomeActivity();
                act.getLocation();
                fingers = 0;
                break;

            case MotionEvent.ACTION_DOWN:
                handleView.postDelayed(runnable, DELAY);
                fingers = 1;
                break;

        }
        if (fingers > 1) {
            disableScrolling();
        } else if (fingers < 1) {
            enableScrolling();
        }
        if (fingers > 1) {
            return scaleGestureDetector.onTouchEvent(ev);
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }

    private void enableScrolling() {
        if (googleMap != null && !googleMap.getUiSettings().isScrollGesturesEnabled()) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    googleMap.getUiSettings().setAllGesturesEnabled(true);
                }
            }, 50);
        }
    }

    private void disableScrolling() {
        handler.removeCallbacksAndMessages(null);
        if (googleMap != null && googleMap.getUiSettings().isScrollGesturesEnabled()) {
            googleMap.getUiSettings().setAllGesturesEnabled(false);
        }
    }
}
