package com.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.Dialog.DialogConfirmation;
import com.Dialog.DialogRequestConfirmation;
import com.adapter.SlideUpPagerAdapter;
import com.bean.CabsBean;
import com.rovecab.HomeActivity;
import com.rovecab.R;
import com.rovecab.add_payment;
import com.service.AsyncTaskListener;
import com.service.CustomButton;
import com.service.CustomText;
import com.service.HttpAsync;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sujith on 16-05-2016.
 */
public class CarSpecificationFragment extends Fragment implements AsyncTaskListener {

    public static ViewPager pager = null;
    public static ArrayList<NameValuePair> nameValuePairs = null;
    public static int position = 0;
    private CustomButton btnRequestRide = null;
    private TextView tripFare = null;
    private TextView seat = null;
    private TextView luggage = null;
    private CustomText cabName = null;
    private CheckBox childSeat = null;
    private ProgressBar progressBar = null;
    private ArrayList<CabsBean> cabsArray = null;
    private String userId = null;
    private String key = null;

    private String addOnsId1 = null;
    private String addOnsId2 = null;
    private int hex = 0;
    private int baseFare = 0;
    private int addOnsFare1 = 0;
    private int addOnsFare2 = 0;
    private int price1 = 0;   //addOn prices
    private int price2 = 0;   // addOn prices
    private String addOns1 = null;
    private String addOns2 = null;
    private int cabId = 0;
    private boolean apiLoaded = false;

    private Context context = null;
    private AsyncTaskListener listener = null;
    private ProgressDialog progressDialog = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_specification, container, false);
        btnRequestRide = (CustomButton) view.findViewById(R.id.request_ride);
        seat = (TextView) view.findViewById(R.id.seats);
        luggage = (TextView) view.findViewById(R.id.luggage);
        pager = (ViewPager) view.findViewById(R.id.pager);
        tripFare = (TextView) view.findViewById(R.id.fare);
        cabName = (CustomText) view.findViewById(R.id.cab_name);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        childSeat = (CheckBox) view.findViewById(R.id.child_seat);

        context = getActivity();
        listener = CarSpecificationFragment.this;
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching card details");
        progressDialog.setIndeterminate(false);

        Bundle bundle = this.getArguments();
        userId = bundle.getString("id");
        position = bundle.getInt("position");
        key = bundle.getString("key");
        cabsArray = (ArrayList<CabsBean>) bundle.getSerializable("cabs");


        loadApi(position);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                loadApi(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        childSeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    price1 = addOnsFare1;
                    addOns1 = addOnsId1;
                    tripFare.setText((char) hex +"" + (baseFare + price1 + price2));
                } else {
                    price1 = 0;
                    addOns1 = "";
                    tripFare.setText((char) hex +"" + (baseFare + price1 + price2));
                }
            }
        });


        btnRequestRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("uId", userId));
                nameValuePairs.add(new BasicNameValuePair("source", String.valueOf(1)));
                nameValuePairs.add(new BasicNameValuePair("key", key));
                if(addOns1.length()>0 && addOns2.length()<1){
                    nameValuePairs.add(new BasicNameValuePair("addOns", addOns1));
                }
                else if(addOns1.length()<1 && addOns2.length()>0){
                    nameValuePairs.add(new BasicNameValuePair("addOns", addOns2));
                }
                else if(addOns1.length()>0 && addOns2.length()>0){
                    nameValuePairs.add(new BasicNameValuePair("addOns", addOns1+ "," +addOns2));
                }

                DialogRequestConfirmation dialogConfirm = new DialogRequestConfirmation(context, listener, nameValuePairs);
                dialogConfirm.show();
            }
        });
        return view;
    }

    @Override
    public void onTaskCancelled(String data) {

    }

    @Override
    public void onTaskComplete(String result, String tag) {
        try {
            JSONObject jobj = new JSONObject(result);
            if (jobj != null) {

                if (tag.equalsIgnoreCase("details")) {
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 1) {
                        progressBar.setVisibility(View.GONE);
                        pager.setVisibility(View.VISIBLE);
                        JSONObject jsonObject = jobj.getJSONObject("carDetails");
                        if (jsonObject != null) {
                            cabId = jsonObject.getInt("id");
                            hex = Integer.parseInt(jsonObject.getString("currencySign"), 16);
                            tripFare.setText((char) hex + "" + jsonObject.getInt("fare"));
                            seat.setText(String.valueOf(jsonObject.getInt("seat")));
                            luggage.setText(String.valueOf(jsonObject.getInt("luggage")));
                            cabName.setText(jsonObject.getString("modelName"));
                            baseFare = jsonObject.getInt("fare");
                            childSeat.setChecked(false);
                            addOns1 = "";
                            addOns2 = "";
                            if (!apiLoaded) {
                                SlideUpPagerAdapter adapter = new SlideUpPagerAdapter(getFragmentManager(), cabsArray, position, jsonObject.getString("image"));
                                pager.setAdapter(adapter);
                                pager.setCurrentItem(position, true);
                                apiLoaded = true;
                            }

                        }

                        JSONArray jarr = jobj.getJSONArray("addOns");
                        if (jarr.length() > 0 && jarr.length() == 2) {
                            JSONObject job1 = jarr.optJSONObject(0);
                            JSONObject job2 = jarr.optJSONObject(1);
                            addOnsId1 = String.valueOf(job1.optInt("id"));
                            addOnsId2 = String.valueOf(job2.optInt("id"));
                            addOnsFare1 = job1.getInt("price");
                            addOnsFare2 = job2.getInt("price");
                            childSeat.setEnabled(true);
                        } else if (jarr.length() > 0 && jarr.length() < 2) {
                            JSONObject job1 = jarr.optJSONObject(0);
//                        JSONObject job2 = jarr.optJSONObject(1);
                            addOnsId1 = String.valueOf(job1.optInt("id"));
                            addOnsFare1 = job1.getInt("price");
//                        addOnsId2 = String.valueOf(job2.optInt("id"));
                            childSeat.setEnabled(true);
                        } else {
                            childSeat.setEnabled(false);
                            addOnsFare1 = 0;
                            addOnsFare2 = 0;
                        }
                    }
                } else if (tag.equalsIgnoreCase("request-ride")) {
                    if (jobj.getString("status").equalsIgnoreCase("success") && jobj.getInt("code") == 11) {
                        HomeActivity.fragmentManager.beginTransaction().remove(HomeActivity.fragmentManager.findFragmentById(R.id.fragments_cars)).commit();
                        progressDialog.dismiss();
                        HomeActivity.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        HomeActivity.fragmentManager.popBackStack();
                        DialogConfirmation confirmation = new DialogConfirmation(context);
                        confirmation.show();
                    } else if (jobj.getString("status").equalsIgnoreCase("error") && jobj.getInt("code") == 100) {
                        progressDialog.dismiss();
                        //no meyment added
                        Intent myint = new Intent(context, add_payment.class);
                        myint.putExtra("request", true);
                        startActivity(myint);
                    } else {
                        Toast.makeText(context, jobj.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadApi(int position) {
        nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("id", String.valueOf(cabsArray.get(position).getId())));
        nameValuePairs.add(new BasicNameValuePair("uId", userId));
        nameValuePairs.add(new BasicNameValuePair("key", key));
        new HttpAsync(context, listener, context.getString(R.string.BASE_URL) + "service/cardetails", nameValuePairs, 2, "details").execute();

    }
}
