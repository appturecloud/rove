package com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bean.CabsBean;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.rovecab.R;

import java.util.List;

/**
 * Created by sujith on 09-05-2016.
 */
public class SliderAdapter extends Fragment {

    public static ImageView cabImage = null;
    public static Context context = null;
    private int position = 0;
    private String imageUrl = null;
    private RelativeLayout relativeLayout = null;
    //    private CardView cardAddons = null;
    private List<CabsBean> bean = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sliding_adapter, null);
//        listItems = (ListView) view.findViewById(R.id.list_item);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.img_see_details);
//        cardAddons = (CardView) view.findViewById(R.id.card_addons);
        cabImage = (ImageView) view.findViewById(R.id.cab_image);

        context = getActivity();

        Bundle bundle = this.getArguments();
        position = bundle.getInt("position");
        imageUrl = bundle.getString("id");
        bean = (List<CabsBean>) bundle.getSerializable("cabs");
        UrlImageViewHelper.setUrlDrawable(cabImage, context.getString(R.string.BASE_URL) + "upload/cars/" + bean.get(position).getImageName());

        return view;
    }
}
