package com.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rovecab.R;

/**
 * Created by sujith on 20-06-2016.
 */
public class PlaceholderFragment extends Fragment {

    public static final String EXTRA_POSITION = "EXTRA_POSITION";
    private ImageView imgSplashCar = null;

    private Animation jump = null;
    private Animation fallDown = null;
    private Long duration = Long.valueOf(2000);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int position = getArguments().getInt(EXTRA_POSITION);
        LinearLayout localRelativeLayout = (LinearLayout) inflater.inflate(R.layout.fragment_intro_page, container, false);

        ImageView image = (ImageView) localRelativeLayout.findViewById(R.id.image);
        imgSplashCar = (ImageView) localRelativeLayout.findViewById(R.id.image);
        jump = AnimationUtils.loadAnimation(getActivity(), R.anim.jumping_anim);
        fallDown = AnimationUtils.loadAnimation(getActivity(), R.anim.jumping_down);

        Animation slide = null;
//        final Animation jump  = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
//                           Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f);
//
//        jump.setDuration(duration);
//        jump.setFillAfter(true);
//        jump.setFillEnabled(true);
//        jump.setRepeatCount(Animation.INFINITE);

        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -0.8f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        slide.setDuration(duration);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }


            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgSplashCar.clearAnimation();
                imgSplashCar.setAnimation(jump);
            }
        });

        jump.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                imgSplashCar.startAnimation(fallDown);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
//                imgSplashCar.clearAnimation();

            }
        });

        fallDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgSplashCar.startAnimation(jump);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });


        imgSplashCar.startAnimation(slide);

        return localRelativeLayout;
    }

}
